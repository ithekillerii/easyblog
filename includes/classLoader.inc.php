<?php
/**
 * The Class Loader for the application with the config loader
 */
//######################### Path Constants #########################
define('classPath','includes/class/');
define('pagePath','includes/pages/');
// ######################### Base Includes #########################
require_once(basePath.'includes/tokenManagment.php');
//######################### Invoke Lazy Classloader #########################
spl_autoload_register(function ($class){
    if(preg_match('/Page/',$class)){
        require_once (basePath.pagePath.$class.'.class.php');
    }else{
        require_once (basePath.classPath.$class.'.class.php');
    }
});

//######################### Parse INI File and define ini Constants #########################
$config = parse_ini_file(basePath.'easyBlog.ini',true);
define('db_host',$config['database']['host']);
define('db_user',$config['database']['user']);
define('db_password',$config['database']['password']);
define('db_database',$config['database']['database']);
define('cfg_parser_useParsedown',$config['parser']['usePasedown']);
define('cfg_deleteFromServer',$config['global']['deleteFromServer']==='1');
