<?php
/**
 * Backend Router
 */
switch($_GET['action']){
    case 'users':
        $page = new UsersBackendPage($actualUser,2);
        break;
    case 'blogs':
        $page = new BlogsBackendPage($actualUser,3);
        break;
    case 'posts':
        $page = new PostsBackendPage($actualUser,4);
        break;
    case 'comments':
        $page = new CommentsBackendPage($actualUser,5);
        break;
    case 'images':
        $page = new ImagesBackendPage($actualUser,6);
        break;
    case 'stats':
        //Not used Yet
        break;
    case 'logout':
        logout(null,0);
        break;
    default:
        $page = new DashBoardPage($actualUser,0);
}