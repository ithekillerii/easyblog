<?php
/**
 * Generate new unique sha1 token from time + random number
 * @param $formName String Name of the form
 */
function genToken($formName){
    $_SESSION[$formName.'_token'] = sha1(uniqid(microtime().rand(0,999999)));
}

/**
 * Check if the token is correct or not
 * @param $token String token received form the form
 * @param $formName String Name of the form
 * @return bool Is the token correct or not
 */
function checkToken($token,$formName){
    return isset($_SESSION[$formName.'_token']) && $_SESSION[$formName.'_token']==$token;
}

/**
 * Get the actual token
 * @param $formName String token received form the form
 * @return String The Token
 */
function getToken($formName){
    return $_SESSION[$formName.'_token'];
}

/**
 * Destroy the session and the session cookie
 */
function destroySession(){
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000, $params["path"],
            $params["domain"], $params["secure"], $params["httponly"]
        );
    }
    $_SESSION[] = array();
    session_destroy();
}