<?php

/**
 * Class User
 * Represent an User Entity
 */
class User {

    private $hasChanged = false;
    private $idUser;
    private $Username;
    private $dtFirstName;
    private $dtLastName;
    private $TSLastLogin;
    private $locked;
    private $backend;

    //#######################################################################################
    //Static methods

    /**
     * Check Username an Email if they are unique in database
     * @param $name string username
     * @param $email string email
     * @return int 0 = ok , 1 = username taken , 2 = email taken
     */
    public static function check($name,$email){
        $db = Database::iniStandard();
        $sql_check = "SELECT dtUsername, dtEmail FROM tblUser";
        $stm = $db->getConnection()->query($sql_check);
        $res = $stm->fetchAll(PDO::FETCH_NUM);
        foreach($res as $val){
            if(in_array($name,$val)) return 1;
            elseif (in_array($email,$val)) return 2;
        }
        return 0;
    }

    /**
     * Salts and hashes the Password
     * @param $pw String Password
     * @param $salt String Salt
     * @return string Salted and hashed Password
     */
    public final static function hashPassword($pw,$salt){
        return sha1($salt.$pw);
    }

    /**
     * Generate a random Salt
     * @return string Salt
     */
    public final static function genSalt(){
        return substr(md5(microtime()),20);
    }

    //#######################################################################################
    //Constructor and Static creation methods

    /**Create a new User from Database with the id.
     * @param $idUser int | string idUser in database
     * @return User the new User from the database
     */
    public static function createFromID($idUser){
        $db = Database::iniStandard();
        $sql_selectPost = 'SELECT * FROM tblUser WHERE idUser = :userID';
        $stm = $db->getConnection()->prepare($sql_selectPost);
        $stm->bindValue(':userID',$idUser,PDO::PARAM_INT);
        $stm->execute();
        $res = $stm->fetch(PDO::FETCH_ASSOC);
        if(count($res) !=0){
            return User::createFromDBRes($res);
        }else{
            return null;
        }
    }

    /**Create a new User from Database Resource
     * @param $res array Database Resource with SELECT idUser,dtUsername,dtFirstName,dtLastName,dtTSLastLogin,dtLocked,dtBackend and FETCH_ASSOC
     * @return User The user from the database
     */
    public static function createFromDBRes($res){
        return new User($res['idUser'],$res['dtUsername'],$res['dtFirstName'],$res['dtLastName'],$res['dtTSLastLogin'],$res['dtLocked'],$res['dtBackend']);
    }

    /**Add a new User and a new Blog to the Database
     * @param string $username The Username
     * @param string $encpw The encryped Password
     * @param string $salt The salt
     * @param string $email The Email
     * @param string $firstName Firtsname of the user
     * @param string $lastName Lastname of the user
     * @param string $blogName Name of the Blog
     * @param string $blogDesc Desc of the Blog
     * @param null|string $userType the user type
     * @param null|string $userState the user state
     */
    public static function addToDB($username, $encpw, $salt, $email, $firstName,$lastName,$blogName,$blogDesc,$userType = null,$userState = null){

        $db = Database::iniStandard();

        $advUser = isset($userType,$userState);

        $sql_insertUser = 'INSERT INTO tblUser(dtUsername, dtPassword, dtSalt, dtEmail, dtFirstName, dtLastName'.($advUser?',dtBackend ,dtLocked) ':') ').
                                                               'VALUES (:username,:password,:salt,:email,:firstName,:lastName'.($advUser?',:type ,:state':'').')';
        $stm_ins = $db->getConnection()->prepare($sql_insertUser);
        $stm_ins->bindValue(':username',$username);
        $stm_ins->bindValue(':password',$encpw);
        $stm_ins->bindValue(':salt',$salt);
        $stm_ins->bindValue(':email',$email);
        $stm_ins->bindValue(':firstName',$firstName);
        $stm_ins->bindValue(':lastName',$lastName);
        if($advUser){
            $stm_ins->bindValue(':type',$userType);
            $stm_ins->bindValue(':state',$userState);
        }
        $stm_ins->execute();

        $sql_insertBlog = 'INSERT INTO tblBlog(dtTitle, dtDescription,fiUser)
                                                               VALUES (:title,:descr,(SELECT idUser FROM tblUser WHERE dtUsername = :usrname));';
        $stm_insBlog = $db->getConnection()->prepare($sql_insertBlog);
        $stm_insBlog->bindValue(':usrname',$username);
        $stm_insBlog->bindValue(':title',$blogName);
        $stm_insBlog->bindValue(':descr',$blogDesc);
        $stm_insBlog->execute();
    }

    /**
     * User constructor.
     * @param $idUser int id from db
     * @param $Username string login name
     * @param $dtFirstName string name
     * @param $dtLastName string name2
     * @param $TSLastLogin string Last Login time
     * @param $locked string locked?
     * @param $backend string backendUser?
     */
    public function __construct($idUser, $Username, $dtFirstName, $dtLastName, $TSLastLogin, $locked, $backend)
    {
        $this->idUser = $idUser;
        $this->Username = $Username;
        $this->dtFirstName = $dtFirstName;
        $this->dtLastName = $dtLastName;
        $this->TSLastLogin = $TSLastLogin;
        $this->locked = $locked;
        $this->backend = $backend;
    }

    //END
    //#########################################################################

    /**
     * Generate an html table row
     * @param $showId bool Display id field
     * @return string html table row
     */
    public function toHTMLTableRow($showId){
        $ar = (array) $this;
        if(!$showId){
            unset($ar[0]);
        }
        return '<tr><td>'.implode('</td><td>',$ar).'</td></tr>';
    }

    /**
     * Get the FirstName of the user
     * @return string FirstName of the user
     */
    public function getDtFirstName()
    {
        return $this->dtFirstName;
    }

    /**
     * Get the LastName of the user
     * @return string LastName of the user
     */
    public function getDtLastName()
    {
        return $this->dtLastName;
    }

    /**
     * Get the username
     * @return string username
     */
    public function getUsername()
    {
        return $this->Username;
    }

    /**
     * Get the user id
     * @return int user id
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Check if user is locked
     * @return bool User Locked?
     */
    public function isLocked()
    {
        return $this->locked == 'true';
    }

    /**
     * Get the User state
     * @return string User State
     */
    public function getUserState(){
        return $this->locked;
    }

    /**
     * Is it a Backend user
     * @return bool
     */
    public function isBackend()
    {
        return $this->backend == 'true';
    }

    public function getBlogId(){
        $query = "SELECT idBlog FROM tblBlog WHERE fiUser={$this->idUser}";
        return Database::iniStandard()->getConnection()->query($query)->fetch(PDO::FETCH_NUM)[0];
    }

    /**Updates this object and the database
     * @param string $username The username
     * @param string $password the password
     * @param string $email the email
     * @param string $firstname the firstname
     * @param string $lastname the lastname
     * @param string $userType the type of the user true backend , false : frontend only
     * @param string $userState the state of the user false : user can login , true : user locked, unregister : user locked and marked to delete
     * @return bool success
     */
    public function editAndUpdateDataBase($username,$password,$email,$firstname,$lastname,$userType,$userState){
        $query = '';
        $db = Database::iniStandard();
        if(isset($username)){
            $query .= 'dtUsername = '.$db->getConnection()->quote($username);
            $this->Username=$username;
        }
        if(isset($password)){
            $salt = User::genSalt();
            $pw = User::hashPassword($password,$salt);
            $query .= ($query===''?'':', '). 'dtPassword = '.$db->getConnection()->quote($pw).', dtSalt = '.$db->getConnection()->quote($salt);
        }
        if(isset($email)){
            $query .= ($query===''?'':', '). 'dtEmail = '.$db->getConnection()->quote($email);
        }
        if(isset($firstname)){
            $query .= ($query===''?'':', '). 'dtFirstName = '.$db->getConnection()->quote($firstname);
            $this->dtFirstName = $firstname;
        }
        if(isset($lastname)){
            $query .= ($query===''?'':', '). 'dtLastName = '.$db->getConnection()->quote($lastname);
            $this->dtLastName = $lastname;
        }
        if(isset($userType)){
            $query .= ($query===''?'':', '). 'dtBackend = '.$db->getConnection()->quote($userType);
            $this->backend = $userType;
        }
        if(isset($userState)){
            $query .= ($query===''?'':', '). 'dtLocked = '.$db->getConnection()->quote($userState);
            $this->locked = $userState;
        }
        if($query !== ''){
            $query .= " WHERE idUser = ".$db->getConnection()->quote($this->idUser,PDO::PARAM_INT);
            $query = 'UPDATE tblUser SET '.$query;
            $db->getConnection()->exec($query);
            return true;
        }
        return false;
    }

    /**
     * Unregister / delete User from EasyBlog
     * @param bool $delete Delete the user from DB else set dtLocked to unregister
     */
    public function unregister($delete){
        $db = Database::iniStandard();
        if($delete){
            $db->getConnection()->exec("DELETE FROM tblUser WHERE idUser = {$this->idUser}");
        }else{
            $db->getConnection()->exec("UPDATE tblUser SET dtLocked='unregister' WHERE idUser = {$this->idUser}");
        }
    }


}