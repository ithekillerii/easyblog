<?php

/**
 * Class Blog
 * Represent a Blog entry
 */
class Blog {

    private $id;
    private $title;
    private $desc;
    private $User;
    private $posts = array();

    /**
     * Create a new blog from ID
     * @param int|string $id The id of the blog
     * @return Blog The blog from the database
     */
    public static function createFromID($id){
        $db = Database::iniStandard();
        $sql_blog = "SELECT idBlog,dtTitle,dtDescription,fiUser,idUser,dtLastName,dtUsername,dtFirstName,dtBackend,dtTSLastLogin,dtLocked FROM tblBlog , tblUser WHERE idBlog = :id AND idUser = fiUser";
        $stm = $db->getConnection()->prepare($sql_blog);
        $stm->bindValue(':id',$id,PDO::PARAM_INT);
        $stm->execute();
        $res = $stm->fetchAll(PDO::FETCH_ASSOC);
        if(count($res)==1){
            $r = $res[0];
            $sql_posts = "SELECT idPost,dtTitle,dtContent FROM tblPost WHERE fiBlog = :idBlog";
            $stm2 = $db->getConnection()->prepare($sql_posts);
            $stm2->bindValue(':idBlog',$id,PDO::PARAM_INT);
            $stm->execute();
            $res2 = $stm2->fetchAll(PDO::FETCH_ASSOC);
            $posts = array();
            foreach($res2 as $postRes){
                $posts[]=new Post($postRes['idPost'],$postRes['dtTitle'],$id,$postRes['dtContent']);
            }
            return new Blog($r['idBlog'],$r['dtTitle'],$r['dtDescription'],User::createFromDBRes($r),$posts);
        }else{
            //Blog Not found
            header("Location: ./index.php?msg=".err_blogNotFound.'&class=alert-danger');
        }
    }


    /**
     * Blog constructor.
     * @param $id mixed id of the blog
     * @param $title string Title of the blog
     * @param $desc string Description of the blog
     * @param $User User the Owner of the blog
     */
    public function __construct($id, $title, $desc, $User)
    {
        $this->id = $id;
        $this->title = $title;
        $this->desc = $desc;
        $this->User = $User;
        $db = new Database(db_host,db_user,db_password,db_database);
        $sql_comment = "SELECT idPost,dtTitle,fiBlog FROM tblPost WHERE fiBlog = :blogid";
        $stm = $db->getConnection()->prepare($sql_comment);
        $stm->bindValue(':blogid',$id,PDO::PARAM_INT);
        $stm->execute();
        $res = $stm->fetchAll(PDO::FETCH_ASSOC);
        foreach($res as $r){
            $this->posts[] = Post::createFromRes($r);
        }
    }

    /**
     * Creates html to display the posts
     * @return String Html with the posts
     */
    public function displayPosts(){
        $out = "";
        foreach($this->posts as $p){
            $out .= $p->displayPostHeader();
        }
        return $out;
    }

    /**
     * Get the id of the Blog
     * @return mixed id of the blog
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the Title of the blog
     * @return string Title of the blog
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the Description of the blog
     * @return string Description of the blog
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * Get the Owner of the blog
     * @return User the Owner of the blog
     */
    public function getUser()
    {
        return $this->User;
    }

    /**
     * Add a new post to list
     * @param Post $post
     */
    public function addPost($post){
        $this->posts[] = $post;
    }

    /**
     * Counts the posts in this Blog
     */
    public function getPostsNumber(){
        return count($this->posts);
    }

    /**
     * Counts the Comments in this Blog
     */
    public function getCommentsNumber(){
        $sum = 0;
        foreach($this->posts AS $post){
            $sum = $sum + $post->getCommentNumber();
        }
        return $sum;
    }

    /**
     * Check if user is autorized
     * @param $uid int idUser to be Authorized
     * @return bool
     */
    public function autorized($uid){
        return($this->User->getIdUser() == $uid);
    }

    /**
     * Updates this Object an the Database
     * @param string $title new Title
     * @param string $desc new Description
     * @return bool success
     */
    public function editAndUpdateDataBase($title, $desc){
        $query = '';
        $db = Database::iniStandard();
        if(isset($title)){
            $query .= 'dtTitle = '.$db->getConnection()->quote($title);
            $this->title=$title;
        }
        if(isset($desc)){
            $query .= ($query===''?'':', ').'dtDescription = '.$db->getConnection()->quote($desc);
            $this->desc=$desc;
        }
        if($query !== ''){
            $query .= " WHERE idBlog = ".$db->getConnection()->quote($this->id,PDO::PARAM_INT);
            $query = 'UPDATE tblBlog SET '.$query;
            $res = $db->getConnection()->exec($query);
            return true;
        }
        return false;
    }
}