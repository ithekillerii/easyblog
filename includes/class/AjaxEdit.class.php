<?php

/**
 * Class AjaxEdit
 * Ajax edit class
 */
class AjaxEdit
{

    const MAXSIZE = 2097152;
    public static $ALLOWED_FileExt = array('gif','jpg','png','bmp');
    public static $ALLOWED_MimeTypes = array('image/gif','image/jpeg','image/png','image/bmp');

    const SHOW_POST_PREFIX = 'Show Post: ';
    const SHOW_COMMENT_PREFIX = 'Show Comment: ';

    /**
     * Takes and Handle the incoming CRUD requests
     */
    public function execCRUD()
    {
        if (isset($_POST['submit_popup'])) {
            $in_id = filter_input(INPUT_GET, 'cid', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
            $in_id2 = filter_input(INPUT_POST, 'id_popup', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
            if (isset($in_id, $in_id2) && $in_id === $in_id2 && checkToken($_POST['token_popup'], 'crudEdit')) {
                switch ($_GET['crud']) {
                    case 'edit':
                        $this->doEditCRUD($in_id, $_GET['type']);
                        break;
                    case 'del':
                        $this->doDelCRUD($in_id, $_GET['type']);
                        break;
                    case 'add':
                        $this->doAddCRUD($_GET['type']);
                        break;
                    default:
                        break;
                }
            } else sendHeader(400);
        } else {
            genToken('crudEdit');
            $in_id = filter_input(INPUT_GET, 'cid', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
            if (!isset($in_id)) sendHeader(400);
            switch ($_GET['crud']) {
                case 'show':
                    $this->printShowCRUD($in_id, $_GET['type']);
                    break;
                case 'edit':
                    $this->printEditCRUD($in_id, $_GET['type']);
                    break;
                case 'del':
                    $this->printDelCRUD($in_id, $_GET['type']);
                    break;
                case 'add':
                    $this->printAddCrud($_GET['type']);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Prints the Show Dialog with a record from the respective type
     * @param $id int | string ID of the Record
     * @param $type string Type of record to be shown (post, user, comment or image)
     */
    private function printShowCRUD($id, $type)
    {
        switch ($type) {
            case 'post':
                $post = Post::createFromID($id);
                Dialog::printShowDialog(AjaxEdit::SHOW_POST_PREFIX . $post->getTitle(), $post->toHtml());
                break;
            case 'comment':
                $comment = Comment::createFromID($id);
                Dialog::printShowDialog(AjaxEdit::SHOW_COMMENT_PREFIX . $comment->getTitle(), $comment->getContent());
                break;
            case 'image':
                $html = '<p>Show Image</p><img src="getImage.php?id=' . $id . '" alt="Image not Found">';
                Dialog::printShowDialog("Show Image", $html);
                break;
            default:
                sendHeader(400);
                break;
        }
    }

    /**
     * Add a new Record for type (for backend only)
     * @param $type string Type of record to be shown (post, user, comment or image)
     */
    private function doAddCRUD($type)
    {
        $user = unserialize($_SESSION['user']);
        if ($user->isBackend() || $type === 'image') {
            switch ($type) {
                case 'user':
                    $in_username = filter_input(INPUT_POST, 'popup_username', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                    $in_pass = filter_input(INPUT_POST, 'popup_password', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                    $in_passRe = filter_input(INPUT_POST, 'popup_password_retype', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                    $in_email = filter_input(INPUT_POST, 'popup_email', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                    $in_firstname = filter_input(INPUT_POST, 'popup_firstname', FILTER_SANITIZE_STRING);
                    $in_lastname = filter_input(INPUT_POST, 'popup_lastname', FILTER_SANITIZE_STRING);
                    $in_userType = filter_input(INPUT_POST, 'popup_usertype', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                    $in_userState = filter_input(INPUT_POST, 'popup_userState', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                    $in_blogName = filter_input(INPUT_POST, 'popup_blogTitle', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                    $in_blogDesc = filter_input(INPUT_POST, 'popup_blogDesc', FILTER_SANITIZE_STRING);

                    if (in_array($in_userType, array('true', 'false')) && in_array($in_userState, array('false', 'true', 'unregister'))) {
                        if (isset($in_username, $in_email, $in_pass, $in_blogName)) {
                            $checkRes = User::check($in_username, $in_email);
                            if ($checkRes === 0) {
                                if ($in_pass === $in_passRe) {
                                    $salt = User::genSalt();
                                    $encPw = User::hashPassword($in_pass, $salt);
                                    User::addToDB($in_username, $encPw, $salt, $in_email, $in_firstname, $in_lastname, $in_blogName, $in_blogDesc, $in_userType, $in_userState);
                                } else {
                                    echo '<div class="alert alert-danger">The Passwords doesn\'t match</div>';
                                }
                            } else if ($checkRes === 1) {
                                echo '<div class="alert alert-danger">Username already taken</div>';
                            } else {
                                echo '<div class="alert alert-danger">Email already taken</div>';
                            }
                        } else {
                            echo '<div class="alert alert-danger">Not all required values are set (username, email, password, blog Title)</div>';
                        }
                    } else {
                        echo '<div class="alert alert-danger">The User has not been Added due to hacky</div>';
                    }
                    break;
                case 'post':
                    $in_uid = filter_input(INPUT_POST,'popup_fiUser',FILTER_SANITIZE_STRING);
                    $in_title = filter_input(INPUT_POST, 'title_popup', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                    $in_content = filter_input(INPUT_POST, 'text_popup', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                    $user = User::createFromID($in_uid);
                    if(isset($user)){
                        Post::createAndAddToDB($user->getBlogId(),$in_title,$in_content);
                    }else{
                        echo '<div class="alert alert-danger">The Post has not been Added due to hacky</div>';
                    }
                    break;
                case 'comment':
                    $in_uid = filter_input(INPUT_POST,'popup_fiUser',FILTER_SANITIZE_STRING);
                    $in_pid = filter_input(INPUT_POST,'popup_fiPost',FILTER_SANITIZE_STRING);
                    $in_title = filter_input(INPUT_POST, 'title_popup', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                    $in_content = filter_input(INPUT_POST, 'text_popup', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                    $user = User::createFromID($in_uid);
                    $post = Post::createFromID($in_pid);
                    if(isset($user)){
                        Comment::createAndAddToDB($in_title,$in_content,$post,$user);
                    }else{
                        echo '<div class="alert alert-danger">The Comment has not been Added due to hacky</div>';
                    }
                    break;
                case 'image':
                    $in_cap = filter_input(INPUT_POST,'caption_popup',FILTER_SANITIZE_STRING);
                    if(isset($_FILES['file'])){
                        $imgUploader = new FileUploadManagement('file');
                        $imgUploader->setMaxSize(self::MAXSIZE);
                        $imgUploader->setWLExtensions(self::$ALLOWED_FileExt);
                        $imgUploader->setWLMimeType(self::$ALLOWED_MimeTypes);
                        $ext = $imgUploader->getFileExt();
                        $res = $imgUploader->moveFile(basePath.'images/upload',$ext,true);
                        if (unserialize($_SESSION['user'])->isBackend() && isset($_GET['adv'])){
                            $in_uid = $in_uid = filter_input(INPUT_POST,'popup_fiUser',FILTER_SANITIZE_STRING);
                            $user = User::createFromID($in_uid);
                            if(!isset($user)) $user = unserialize($_SESSION['user']);
                        }else{
                            $user = unserialize($_SESSION['user']);
                        }
                        if(isset($res)){
                            $img = Image::createAndAddToDB($res['userFile'],$in_cap,$res['serverFile'],$user);
                            echo '<div class="alert alert-success">The Image has been Added</div>';
                        }else{
                            echo '<div class="alert alert-danger">The Image has not been Added</div>';
                        }

                    }
                    break;
                default:
                    sendHeader(400);
                    break;
            }
        }
    }

    /**
     * Execute the Edit Crud
     * @param $id int | string ID of the Record
     * @param $type string Type of record to be shown (user, post, comment or image)
     */
    private function doEditCRUD($id, $type)
    {
        $in_title = filter_input(INPUT_POST, 'title_popup', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
        $in_content = filter_input(INPUT_POST, 'text_popup', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
        $usr = unserialize($_SESSION['user']);
        switch ($type) {
            case 'user':
                $user = User::createFromID($id);
                $in_username = filter_input(INPUT_POST, 'popup_username', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                $in_pass = filter_input(INPUT_POST, 'popup_password', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                $in_passRe = filter_input(INPUT_POST, 'popup_password_retype', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                $in_email = filter_input(INPUT_POST, 'popup_email', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                $in_firstname = filter_input(INPUT_POST, 'popup_firstname', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                $in_lastname = filter_input(INPUT_POST, 'popup_lastname', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                $in_userType = filter_input(INPUT_POST, 'popup_usertype', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                $in_userState = filter_input(INPUT_POST, 'popup_userState', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                if (in_array($in_userType, array('true', 'false')) && in_array($in_userState, array('false', 'true', 'unregister'))) {
                    if($in_pass === $in_passRe){
                        if ($user->editAndUpdateDataBase($in_username, $in_pass, $in_email, $in_firstname, $in_lastname, $in_userType, $in_userState)) {
                            echo '<div class="alert alert-success">The User has been Edited</div>';
                        } else {
                            echo '<div class="alert alert-waring">The User has not been Edited</div>';
                        }
                    }else{
                        echo '<div class="alert alert-danger">The Passwords dosen\'t match</div>';
                    }
                } else {
                    echo '<div class="alert alert-danger">The User has not been Edited due to hacky</div>';
                }
                break;
            case 'post':
                $post = Post::createFromID($id);
                if ($post->getBlogId() == $_SESSION['blogID'] || $usr->isBackend()) {
                    if ($post->editAndUpdateDataBase($in_title, $in_content)) {
                        echo '<div class="alert alert-success">The Post has been Edited</div>';
                    } else {
                        echo '<div class="alert alert-waring">The Post has not been Edited</div>';
                    }
                } else {
                    sendHeader(401);
                }
                break;
            case 'comment':
                $blog = Comment::createFromID($id);
                if (($blog->getUser()->getIdUser() === $usr->getIdUser()) || $usr->isBackend()) {
                    if ($blog->editAndUpdateDataBase($in_title, $in_content)) {
                        echo '<div class="alert alert-success">The Comment has been Edited</div>';
                    } else {
                        echo '<div class="alert alert-waring">The Comment has not been Edited</div>';
                    }
                } else {
                    sendHeader(401);
                }
                break;
            case 'image':
                $in_caption = filter_input(INPUT_POST, 'caption_popup', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                $in_name = filter_input(INPUT_POST, 'name_popup', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                $image = Image::createFromID($id);
                if (($image->getUser()->getIdUser() === $usr->getIdUser()) || $usr->isBackend()) {
                    if ($image->updateMeAndDatabase($in_caption,$in_name)) {
                        echo '<div class="alert alert-success">The Image has been Edited</div>';
                    } else {
                        echo '<div class="alert alert-waring">The Image has not been Edited</div>';
                    }
                } else {
                    sendHeader(401);
                }
                break;
            case 'blog':
                $blog = Blog::createFromID($id);
                if (($blog->getUser()->getIdUser() === $usr->getIdUser()) || $usr->isBackend()) {
                    if ($blog->editAndUpdateDataBase($in_title, $in_content)) {
                        echo '<div class="alert alert-success">The Blog has been Edited</div>';
                    } else {
                        echo '<div class="alert alert-waring">The Blog has not been Edited</div>';
                    }
                } else {
                    sendHeader(401);
                }
                break;
            default:
                sendHeader(400);
                break;
        }
    }

    /**
     * Execute the delete Crud
     * @param $id int | string ID of the Record
     * @param $type string Type of record to be shown (user, post, comment or image)
     */
    private function doDelCRUD($id, $type)
    {
        switch ($type) {
            case 'user':
                if (unserialize($_SESSION['user'])->isBackend()) {
                    $user = User::createFromID($id);
                    $user->unregister(true);
                    echo '<div class="alert alert-success">The User has been Deleted</div>';
                } else {
                    sendHeader(403);
                }
                break;
            case 'post':
                $post = Post::createFromID($id);
                if ($post->deleteMe()) {
                    echo '<div class="alert alert-success">The Post has been Deleted</div>';
                } else {
                    echo '<div class="alert alert-waring">The Post has not been Deleted</div>';
                }
                break;
            case 'comment':
                $comment = Comment::createFromID($id);
                if ($comment->deleteMe()) {
                    echo '<div class="alert alert-success">The Comment has been Deleted</div>';
                } else {
                    echo '<div class="alert alert-waring">The Comment has not been Deleted</div>';
                }
                break;
            case 'image':
                $image = Image::createFromID($id);
                $image->deleteMe(cfg_deleteFromServer);
                break;
            default:
                sendHeader(400);
                break;
        }
    }


    /**
     * Prints the Add Dialog for a record from the respective type
     * @param $type string Type of record to be shown (post, user, comment or image)
     */
    private function printAddCrud($type)
    {

        switch ($type) {
            case 'user':
                if (!unserialize($_SESSION['user'])->isBackend()) sendHeader(401);
                Dialog::printAddEditUser('Add User', 'Add', 'crudEdit', '', true);
                break;
            case 'post':
                if (!unserialize($_SESSION['user'])->isBackend()) sendHeader(401);
                $options = Dialog::getOptions('popup_fiUser', 'Select User', Database::iniStandard()->getOptions('tblUser', 'idUser', array('dtUsername')));
                Dialog::printAddEditPopup('Add Post', 'Add', 'crudEdit', '', false, -1, $options);
                break;
            case 'comment':
                if (!unserialize($_SESSION['user'])->isBackend()) sendHeader(401);
                $options = Dialog::getOptions('popup_fiUser', 'Select User', Database::iniStandard()->getOptions('tblUser', 'idUser', array('dtUsername')));
                $options .= Dialog::getOptions('popup_fiPost', 'Select Post', Database::iniStandard()->getOptions('tblPost', 'idPost', array('dtTitle')));
                Dialog::printAddEditPopup('Add Comment', 'Add', 'crudEdit', '', false, -1, $options);
                break;
            case 'image':
                $options = null;
                if (unserialize($_SESSION['user'])->isBackend() && isset($_GET['adv'])){
                    $options = Dialog::getOptions('popup_fiUser', 'Select User', Database::iniStandard()->getOptions('tblUser', 'idUser', array('dtUsername')));
                }
                Dialog::printAddEditImage('Add Image', 'Add', 'crudEdit', '', false, -1, $options);
                break;
            default:
                sendHeader(400);
                break;
        }
    }

    /**
     * Prints the Edit Dialog with a record from the respective type
     * @param $id int | string ID of the Record
     * @param $type string Type of record to be shown (user, post, comment or image)
     */
    private function printEditCRUD($id, $type)
    {
        switch ($type) {
            case 'user':
                $user = User::createFromID($id);
                $userType = $user->isBackend() ? 1 : 0;
                $userState = $user->getUserState() === 'unlocked' ? 0 : $user->getUserState() === 'unregister' ? 2 : 0;
                Dialog::printAddEditUser('Edit User', 'Edit', 'crudEdit', '', false, $id, $userType, $userState);
                break;
            case 'blog':
                Dialog::printAddEditPopup('Edit Blog', 'Edit', 'crudEdit', '', false, $id);
                break;
            case 'post':
                Dialog::printAddEditPopup('Edit Post', 'Edit', 'crudEdit', '', false, $id);
                break;
            case 'comment':
                Dialog::printAddEditPopup('Edit Comment', 'Edit', 'crudEdit', '', false, $id);
                break;
            case 'image':
                Dialog::printAddEditImage('Edit Image', 'Edit', 'crudEdit', '', false, $id);
                break;
            default:
                sendHeader(400);
                break;
        }
    }

    /**
     * Prints the Delete Dialog with a record from the respective type
     * @param $id int | string ID of the Record
     * @param $type string Type of record to be shown (user, post, comment or image)
     */
    private function printDelCRUD($id, $type)
    {
        switch ($type) {
            case 'user':
                Dialog::printConfirmDialog($id, 'Confirm Delete', 'Do you really want to delete the User with the id: ' . $id, 'crudEdit');
                break;
            case 'post':
                Dialog::printConfirmDialog($id, 'Confirm Delete', 'Do you really want to delete the Post with the id: ' . $id, 'crudEdit');
                break;
            case 'comment':
                Dialog::printConfirmDialog($id, 'Confirm Delete', 'Do you really want to delete the Comment with the id: ' . $id, 'crudEdit');
                break;
            case 'image':
                Dialog::printConfirmDialog($id, 'Confirm Delete', 'Do you really want to delete the Image with the id: ' . $id, 'crudEdit');
                break;
            default:
                sendHeader(400);
                break;
        }
    }
}