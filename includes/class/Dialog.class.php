<?php

/**
 * Class Dialog
 * Ajax Dialog / Popup Printer
 */
class Dialog
{

    /**
     * Prints the show Dialog
     * @param string $title Title of the Dialog
     * @param string $content Content of the Dialog
     */
    public static function printShowDialog($title, $content)
    { ?>
        <div id="overlay"></div>
        <div id="popup" class="container-fluid">
            <div class="row">
                <div class="col-md-10"><h3><?= $title ?></h3></div>
                <div class="col-md-2">
                    <button class="closePopup btn btn-default btn-sm"><span
                            class="glyphicon glyphicon-remove"></span> Close
                    </button>
                </div>
            </div>
            <hr>
            <div><?= $content ?></div>
            <hr>
            <a class="closePopup btn btn-lg btn-primary">Close<a/>
        </div>
        <?php
    }

    /**
     * Prints the Add Edit Image Popup
     * @param $title string title of the Popup
     * @param $btnText string Text displayed in button
     * @param $tokenName string Name of the token
     * @param string $msg Message displayed under the Title
     * @param bool $usePostData use post data to auto fill the fields
     * @param null | int $id Add id_popup hidden field
     * @param null|string $options The options for the select reference
     */
    public static function printAddEditImage($title, $btnText, $tokenName, $msg = '', $usePostData = false, $id = null,$options=null)
    {
        ?>
        <div id="overlay"></div>
        <div id="popup" class="container-fluid">
            <div class="row">
                <div class="col-md-10"><h3><?= $title ?></h3></div>
                <div class="col-md-2">
                    <button class="closePopup btn btn-default btn-sm"><span
                            class="glyphicon glyphicon-remove"></span> Close
                    </button>
                </div>
            </div>
            <hr>
            <div id="popUpMsg"><?= $msg ?></div>
            <form id="popupForm" action="" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="imageCap">Caption: </label>
                    <input type="text" id="imageCap"
                           name="caption_popup" <?= $usePostData ? 'value="' . $_POST['caption_popup'] . '"' : 'placeholder="Caption"' ?> >
                </div>
                <?php if($id === -1){?>
                    <div class="form-group">
                        <input type="hidden" name="MAX_FILE_SIZE" value="<?=AjaxEdit::MAXSIZE?>" >
                        <label for="imageUploadFile">File input</label>
                        <input type="file" id="imageUploadFile" name="file" multiple>

                        <p class="help-block">Allowed types: .<?=implode(', .',AjaxEdit::$ALLOWED_FileExt)?></p>
                    </div>
                <?php }else{ ?>
                    <div class="form-group">
                        <label for="imageName">Name: </label>
                        <input id="imageName" type="text" name="name_popup" placeholder="Name">
                    </div>
                <?php } ?>
                <?php if(isset($options)){
                    echo $options;
                }?>
                <div class="form-group">
                    <input id="submitImage" class="confirmPopup btn btn-lg btn-primary" type="submit" value="<?= $btnText ?>"
                           name="submit_popup">
                    <input type="hidden" name="token_popup" value="<?= getToken($tokenName) ?>">
                    <?= isset($id) ? '<input type="hidden" name="id_popup" value="' . $id . '">' : '' ?>
                </div>
            </form>
        </div>
        <?php
    }

    /**
     * Prints the Add Edit Popup
     * @param $title string title of the Popup
     * @param $btnText string Text displayed in button
     * @param $tokenName string Name of the token
     * @param string $msg Message displayed under the Title
     * @param bool $usePostData use post data to auto fill the fields
     * @param null | int $id Add id_popup hidden field
     * @param null|String $options The options for the select reference
     */
    public static function printAddEditPopup($title, $btnText, $tokenName, $msg = '', $usePostData = false, $id = null, $options=null)
    {
        ?>
        <div id="overlay"></div>
        <div id="popup" class="container-fluid">
            <div class="row">
                <div class="col-md-10"><h3><?= $title ?></h3></div>
                <div class="col-md-2">
                    <button class="closePopup btn btn-default btn-sm"><span
                            class="glyphicon glyphicon-remove"></span> Close
                    </button>
                </div>
            </div>
            <hr>
            <div id="popUpMsg"><?= $msg ?></div>
            <form id="popupForm" action="" method="post">
                <div class="form-group">
                    <input type="text"
                           name="title_popup" <?= $usePostData ? 'value="' . $_POST['title_popup'] . '"' : 'placeholder="Title"' ?> >
                </div>
                <div class="form-group">
                <textarea rows="10"
                          name="text_popup" <?= $usePostData ? '>' . $_POST['text_popup'] : 'placeholder="Add Text here" >' ?></textarea>
                </div>
                <?php if(isset($options)){ ?>
                    <?=$options?>
                <?php } ?>
                <div class="form-group">
                    <input class="confirmPopup btn btn-lg btn-primary" type="submit" value="<?= $btnText ?>"
                           name="submit_popup">
                    <input type="hidden" name="token_popup" value="<?= getToken($tokenName) ?>">
                    <?= isset($id) ? '<input type="hidden" name="id_popup" value="' . $id . '">' : '' ?>
                </div>
            </form>
        </div>
        <?php
    }

    /**
     * Prints the Confirm dialog
     * @param int $id Add id_popup hidden field
     * @param $title String Title of the dialog
     * @param $msg String Message of the dialog
     * @param $tokenName string Name of the token
     * @param bool $overlay Displays the Overlay
     * @param string $rejectUrl Url with redirect when no is selected and popup.js not loaded
     */
    public static function printConfirmDialog($id, $title, $msg, $tokenName, $overlay = true, $rejectUrl = '')
    {
        if ($overlay) {
            echo '<div id="overlay"></div>';
        }
        ?>
        <div id="popup" class="container-fluid">
            <div class="row">
                <div class="col-md-10"><h3><?= $title ?></h3></div>
                <div class="col-md-2">
                    <a href="<?= $rejectUrl ?>" class="closePopup btn btn-default btn-sm"><span
                            class="glyphicon glyphicon-remove"></span> Close
                    </a>
                </div>
            </div>
            <hr>
            <div id="popUpMsg"><?= $msg ?></div>
            <form id="popupForm" action="" method="post">
                <div class="form-group">
                    <input class="confirmPopup btn btn-lg btn-success" type="submit" id="confirmPopup" value="Yes"
                           name="submit_popup">
                    &nbsp;&nbsp;&nbsp;<a href="<?= $rejectUrl ?>" class="closePopup btn btn-lg btn-danger">No</a>
                    <input type="hidden" name="token_popup" value="<?= getToken($tokenName) ?>">
                    <input type="hidden" name="id_popup" value="<?= $id ?>">
                </div>
            </form>
        </div>
    <?php }

    /**
     * Print the Crud Tables
     * @param string $table Tablename
     * @param bool $showLink Show the links
     * @param bool $all Show all for backend
     */
    public static function printCrudTable($table, $showLink = false, $all = false)
    {
        $showPK = true;
        $tblattr = 'class="table table-striped tablesorter"';
        $db = Database::iniStandard();
        $editAction = new TableAction('edit', 'edt', 'return "<a class=\"crudEdit btn btn-info btn-sm\" data-id=\"$id\"><span class=\"glyphicon glyphicon-pencil\"></span> Edit</a>";', true);
        $delAction = new TableAction('delete', 'del', 'return "<a class=\"crudDel btn btn-danger btn-sm\" data-id=\"$id\"><span class=\"glyphicon glyphicon-remove\"></span> Delete</a>";', true);
        $exportPDFAction = new TableAction('Export as pdf','pdf','return "<a class=\"crudPDF btn btn-warning btn-sm\" data-id=\"$id\"><span class=\"glyphicon glyphicon-download-alt\"></span> Export as PDF</a>";', true);
        switch ($table) {
            case 'tblPost':
                if($all){
                    $fields = array('dtTitle', 'fiBlog','dtTSCreation');
                    $head = array('Title','Blog','Creation Time');
                }else{
                    $fields = array('dtTitle', 'dtTSCreation');
                    $head = array('Title','Creation Time');
                }
                $db->addTableDescription($table, array('idPost', 'dtTitle', 'dtContent', 'dtTSCreation'));
                $showAction = new TableAction('show', 'post', 'return "<a href=\"index.php?action=post&id=$id\" class=\"crudShow btn btn-success btn-sm\" data-id=\"$id\"><span class=\"glyphicon glyphicon-eye-open\"></span> Show</a>";', true);
                $db->addAction($showAction);
                $db->addAction($editAction);
                $db->addAction($delAction);
                $db->addAction($exportPDFAction);
                $all ? $cond = '' : $cond = "WHERE fiBlog = " . $_SESSION['blogID'];
                echo $db->display($fields, $head, $showPK, $tblattr, $showLink, $cond);
                break;
            case 'tblComment':
                if($all){
                    $fields = array('dtTitle', 'fiUser','fiPost','dtTSCreation');
                    $head = array('Title','idUser','idPost','Creation Time');
                }else{
                    $fields = array('dtTitle', 'dtTSCreation');
                    $head = array('Title','Creation Time');
                }
                $db->addTableDescription($table, array('idComment', 'dtTitle', 'dtContent', 'dtTSCreation'));
                $showAction = new TableAction('show', 'com', 'return "<a href=\"index.php?action=post&id=$id\" class=\"crudShow btn btn-success btn-sm\" data-id=\"$id\"><span class=\"glyphicon glyphicon-eye-open\"></span> Show</a>";', true);
                $db->addAction($showAction);
                $db->addAction($editAction);
                $db->addAction($delAction);
                $db->addAction($exportPDFAction);
                $all ? $cond = '' : $cond = "WHERE fiUser = " . unserialize($_SESSION['user'])->getIdUser();
                echo $db->display($fields,$head, $showPK, $tblattr, $showLink, $cond);
                break;
            case 'tblImage':
                if($all){
                    $fields = array('dtName', 'dtCaption', 'fiUser','dtTSCreation');
                    $head = array('Name','Caption','User','Creation Time');
                }else{
                    $fields = array('dtName', 'dtCaption', 'dtTSCreation');
                    $head = array('Name','Caption','Creation Time');
                }
                $db->addTableDescription($table, array('idImage', 'dtName', 'dtCaption', 'dtTSCreation'));
                $showAction = new TableAction('show', 'simg', 'return "<a href=\"\" class=\"crudShow btn btn-success btn-sm\" data-id=\"$id\"><span class=\"glyphicon glyphicon-eye-open\"></span> Show</a>";', true);
                $db->addAction($showAction);
                $db->addAction($editAction);
                $db->addAction($delAction);
                $all ? $cond = '' : $cond = "WHERE fiUser = " . unserialize($_SESSION['user'])->getIdUser();
                echo $db->display($fields,$head, $showPK, $tblattr, $showLink, $cond);
                break;
            case 'tblUser':
                $fields = array('dtUsername', 'dtEmail', 'dtFirstName', 'dtLastName', 'dtBackend', 'dtLocked', 'dtTSCreation', 'dtTSLastLogin');
                $head   = array('Username', 'Email', 'First Name', 'Last Name', 'Backend', 'Locked', 'Creation Time', 'Last login');
                $db->addTableDescription($table, array('idUser', 'dtUsername', 'dtEmail', 'dtFirstName', 'dtLastName', 'dtBackend', 'dtLocked', 'dtTSCreation', 'dtTSLastLogin'));
                $db->addAction($editAction);
                $db->addAction($delAction);
                $all ? $cond = '' : $cond = "WHERE idUser = " . unserialize($_SESSION['user'])->getIdUser();
                echo $db->display($fields, $head, $showPK, $tblattr, $showLink, $cond);
                break;
            case 'tblBlog':
                $fields = array('dtTitle', 'dtDescription', 'fiUser','dtTSCreation');
                $head = array('Title','Description','User','Creation Time');
                $db->addTableDescription($table, array('idBlog', 'dtTitle', 'dtDescription','dtTSCreation'));
                $db->addAction($editAction);
                $all ? $cond = '' : $cond = "WHERE fiUser = " . unserialize($_SESSION['user'])->getIdUser();
                echo $db->display($fields, $head, $showPK, $tblattr, $showLink, $cond);
                break;
            default:
                break;
        }
    }

    /**
     * Prints the Add User Popup
     * @param $title string title of the Popup
     * @param $btnText string Text displayed in button
     * @param $tokenName string Name of the token
     * @param string $msg Message displayed under the Title
     * @param bool $addDialog Is it am add Dialog or not ?
     * @param int $id Id of the edited User
     * @param int $usertype The type of the actual user 0: frontend, 1: backend
     * @param int $userstate The state of the actual user 0: unlocked, 1: locked, 2: unregistered
     */
    public static function printAddEditUser($title, $btnText, $tokenName, $msg = '', $addDialog = false,$id=-1,$usertype = 0,$userstate = 0)
    { ?>
        <div id="overlay"></div>
        <div id="popup" class="container-fluid">
            <div class="row">
                <div class="col-md-10"><h3><?= $title ?></h3></div>
                <div class="col-md-2">
                    <button class="closePopup btn btn-default btn-sm"><span
                            class="glyphicon glyphicon-remove"></span> Close
                    </button>
                </div>
            </div>
            <hr>
            <div id="popUpMsg"><?= $msg ?></div>
            <form id="popupForm" method="post" action="" class="form-horizontal">
                <h2>Login</h2>
                <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputEmail" name="popup_username" placeholder="Username">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="inputEmail" name="popup_email" placeholder="Email">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="popup_password" id="inputPassword"
                               placeholder="Password">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword2" class="col-sm-2 control-label">Retype Password</label>

                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="popup_password_retype" id="inputPassword2"
                               placeholder=" Retype Password">
                    </div>
                </div>
                <h2>Personal Informations</h2>

                <div class="form-group">
                    <label for="inputFname" class="col-sm-2 control-label">Firstname</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputFname" name="popup_firstname"
                               placeholder="Firstname">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputLname" class="col-sm-2 control-label">Lastname</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputLname" name="popup_lastname"
                               placeholder="Lastname">
                    </div>
                </div>
                <h2>Advanced</h2>
                <div class="form-group">

                </div>
                <div class="form-group">
                    <p><label>User Type</label></p>
                    <p><input <?=$usertype===0?'checked':''?> type="radio" id="userType_front" value="false" name="popup_usertype"> <label for="userType_front">Fronend User</label></p>
                    <p><input <?=$usertype===1?'checked':''?> type="radio" id="userType_back" value="true" name="popup_usertype"> <label for="userType_back">Backend User</label></p>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="userState">User State</label>
                    <div class="col-sm-10"><select id="userState" name="popup_userState">
                        <option <?=$userstate===0?'selected':''?> value="false">Unlocked</option>
                        <option <?=$userstate===1?'selected':''?>value="true">Locked</option>
                        <option <?=$userstate===2?'selected':''?>value="unregister">Unregistered</option>
                    </select></div>
                </div>
                <?php if($addDialog) { ?>
                    <h2>Blog Informations</h2>
                    <div class="form-group">
                        <label for="inputBlogName" class="col-sm-2 control-label">Blog Name</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputBlogName" name="popup_blogTitle"
                                   placeholder="Blog Title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputBlogDesc" class="col-sm-2 control-label">Blog Description</label>

                        <div class="col-sm-10">
                            <textarea class="form-control" id="inputBlogDesc" name="popup_blogDesc"
                                   placeholder="Blog Description"></textarea>
                        </div>
                    </div>
                <?php } ?>
                <div class="form-group">
                    <input type="submit" class="confirmPopup btn btn-lg btn-success" name="submit_popup" value="<?=$btnText?>">
                    <input type="hidden" value="<?=getToken($tokenName)?>" name="token_popup">
                    <?= isset($id) ? '<input type="hidden" name="id_popup" value="' . $id . '">' : '' ?>
                </div>
            </form>
        </div>
        <?php
    }

    /**
     * Get the options for the select in the Dialogs
     * @param string $name Name of the select tag
     * @param string $txt Text to be printed in label
     * @param string $options Options Tag from the database
     * @return string select tag with options
     */
    public static function getOptions($name,$txt,$options){
        return <<< html
        <div class="form-group">
            <label for="$name">$txt</label>
            <select id="$name" name="$name">
                $options
            </select>
        </div>
html;
    }
}