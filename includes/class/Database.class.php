<?php
require_once('TableAction.class.php');

/**
 * Class Database
 * My Database lib to connect to the Database
 */
class Database
{
    const GET_ACTION_IDETIFIER = 'action';

    private $con;
    private $table;
    private $tableDescription;
    private $actions;
    private $buttons = "";

    /**
     * Create a Database with the settings from the ini File
     * @return Database Database created from ini Settings
     */
    public static function iniStandard()
    {
        return new Database(db_host, db_user, db_password, db_database);
    }

    /**
     * Create a new Database
     * @param $server string Hostname of the Database
     * @param $user string Database user
     * @param $password string Database Password
     * @param $database string Database
     * @param string $dbType Type of the DBMS
     */
    public function __construct($server, $user, $password, $database, $dbType = 'mysql')
    {
        $connString = $dbType . ':host=' . $server . ';dbname=' . $database;
        try {
            $this->con = new PDO($connString, $user, $password);
        } catch (PDOException $e) {
            echo '<p class="error"> Error Database not reachable</p>';
        }
        $this->actions = array();
    }

    /**
     * Destructor reset the connection
     */
    public function __destruct()
    {
        $this->con = null;
    }

    /**
     * Get the raw Connection from pdo
     * @return PDO Raw Connection
     */
    public function getConnection()
    {
        return $this->con;
    }

    /**
     * Adds a table description to the database and select the added table
     * @param $table string Tablename from database
     * @param $fields array array With the fields name from database (The field with the id and pk must be the first)
     */
    public function addTableDescription($table, $fields)
    {
        $this->tableDescription[$table] = $fields;
        $this->table = $table;
    }

    /**
     * Gets the Table description of the actual table
     * @return array The table description
     */
    public function getActualTableDescription()
    {
        return $this->tableDescription[$this->table];
    }

    /**
     * Selects the table for further operations
     * @param $table String Table Name
     */
    public function useTable($table)
    {
        $this->table = $table;
    }

    /**
     * Add an action for the Display
     * @param $action TableAction TableAction Object
     */
    public function addAction($action)
    {
        $this->actions[] = $action;
    }

    /**
     * Sets the Buttons displayed at the end of the Table
     * @param $htmlButtons string Html buttons
     */
    public function setButtons($htmlButtons)
    {
        $this->buttons = $htmlButtons;
    }

    /**
     * Displays the database as html Table
     * @param $fields array The name of the fields from the Database to display (* is not allowed)
     * @param $headers array|null The header of the Table (If null use the names from fields)
     * @param bool $showPk displays the primary key in the table
     * @param string $tableAttr string Attributes added to Table
     * @param bool $useLink Uses link or alternative html passed by the action
     * @param null|string $cond SQLConditions to query Database
     * @return string
     */
    public function display($fields, $headers = null, $showPk = true, $tableAttr = '',$useLink = false, $cond = null)
    {
        $queryFields = $this->tableDescription[$this->table][0] . ' ,' . implode(', ', $fields);
        $cond2 = isset($cond)?$cond:'';
        $stm = $this->con->prepare("SELECT $queryFields FROM $this->table $cond2;");
        $stm->execute();
        $res = $stm->fetchAll(PDO::FETCH_NUM);
        $out = '<form method="post"><table '.$tableAttr.'><thead><tr>' . ($showPk ? '<th class="header">ID</th>' : '');
        //Use $headers or $fields as headers
        if (isset($headers)) {
            $loopArray = $headers;
        } else {
            $loopArray = $fields;
        }
        //Add header to Table
        foreach ($loopArray as $head) {
            $out .= "<th class=\"header\">$head</th>";
        }
        // Add actions table Header:
        if (count($this->actions) != 0) {
            $out .= '<th colspan="' . count($this->actions) . '">Actions</th>';
        }
        $out .= '</tr></thead><tbody>';
        //Build Table
        for ($i = 0; $i < count($res); $i++) {
            $ctrl = $this->ctrl((int)$res[$i][0], $useLink);
            $out .= '<tr>';
            $begin = $showPk ? 0 : 1;
            for ($j = $begin; $j < count($res[$i]); $j++) {
                $out .= "<td>{$res[$i][$j]}</td>";
            }
            $out .= "$ctrl</tr>";
        }
        $out .= "</tbody></table>$this->buttons</form>";
        return $out;
    }

    /**
     * Creates the Actions
     * @param $id int id of the element in the table
     * @param $useLink bool Use the link or altHtml
     * @return string Table entry with actions
     */
    private function ctrl($id, $useLink)
    {
        // If no Actions are set exit
        if (count($this->actions) == 0) {
            return '';
        } else {
            $out = '';
            foreach ($this->actions as $ac) {
                $txt = $ac->getText();
                $short = $ac->getShort();
                if (!$useLink) {
                    $md = $ac->getAltHtml($id);
                    $out .= "<td>$md</td>";
                } else {
                    $out .= "<td><a href=\"?".Database::GET_ACTION_IDETIFIER."=$short&id=$id\">$txt</a></td>";
                }
            }
            return $out;
        }
    }

    /**
     * Get all data from actual table
     * @return array Result array with PDO::FETCH_NUM
     */
    public function getAllData()
    {
        $stm = $this->con->query("SELECT * FROM $this->table");
        $res = $stm->fetchAll(PDO::FETCH_NUM);
        return $res;
    }

    /**
     * Count the Records from $table
     * @param string $table Name of the table to be counted
     * @return int|string num of users in db
     */
    public function countRecords($table){
        $sql = "SELECT COUNT(*) FROM $table";
        return $this->con->query($sql)->fetch(PDO::FETCH_NUM)[0];
    }

    /**
     * Returns the Record with the Value in $field
     * @param string $table Name of the table
     * @param string $field
     * @return array Record with highest $field (FETCH_ASSOC)
     */
    public function getRecordWithHighestField($table,$field){
        $sql = "SELECT * FROM $table ORDER BY $field DESC LIMIT 1";
        return $this->con->query($sql)->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Gets a csv Array with every entry is one record from table
     * @return array csv array
     */
    public function getCSVArray()
    {
        $res = $this->getAllData();
        $csv = array();
        foreach ($res as $val) {
            $csv[] = '"' . implode('", "', $val) . '"';
        }
        return $csv;
    }

    /**
     * Get options tags with the from the Database
     * @param string $table name of the table
     * @param string $idField name of the idField from DB
     * @param array $fields Fields to be displayed
     * @return string Options tags
     */
    public function getOptions($table,$idField,$fields){
        $out = '';
        $queryfields = $idField. ' ,' . implode(', ', $fields);
        $stm = $this->con->query('SELECT '.$queryfields.' FROM '.$table);
        $res = $stm->fetchAll(PDO::FETCH_NUM);
        foreach ($res as $val){
            $id=$val[0];
            $txt = implode(' ,',$val);
            $out .= "<option value=\"$id\">$txt</option>\n";
        }
        return $out;
    }
}
