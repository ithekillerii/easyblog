<?php

/**
 * Class TableAction
 * Table Actions used in the CRUD
 */
class TableAction
{

    private $text;
    private $short;
    private $altHtml;
    private $interpretPhp;

    /**
     * Creates a new TableAction
     * @param $text String Action name
     * @param $short String Name of the action
     * @param $altHtml String HTML to be displayed as Alternative to links
     * @param bool $interpretPhp Use eval or not (DANGEROUS)
     */
    public function __construct($text, $short, $altHtml, $interpretPhp = false)
    {
        $this->text = $text;
        $this->short = $short;
        $this->altHtml = $altHtml;
        $this->interpretPhp = $interpretPhp;
    }

    /**
     * Get the text of the Action
     * @return String The text of the Action
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Get the shorthand of the Action
     * @return String The shorthand of the Action
     */
    public function getShort()
    {
        return $this->short;
    }

    /**
     * Get the location of the Image
     * @return string The location of the Image
     */
    public function getAltHtml($id)
    {
        return $this->interpretPhp ? eval($this->altHtml) : $this->altHtml;
    }
}