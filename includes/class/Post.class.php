<?php

/**
 * Class Post
 * Represent a Post entry
 */
class Post {

    private $idPost;
    private $dtTitle;
    private $dtContent;
    private $blogId;
    private $comments  = array();

    /**
    * Create new Post from Database Resource
    * @param $res array Database Recourse from PDO with FETCH_Assoc
    * @param $useContent bool Save also the Content
    * @return Post The created Post
    */
    public static function createFromRes($res,$useContent=false){
        return new Post($res['idPost'],$res['dtTitle'],$res['fiBlog'],$useContent?$res['dtContent']:null);
    }

    /**
     * Create a new Post from Database with the id.
     * @param $idPost int | string idPost in database
     * @param $owner String The name of the User who own the post
     * @return Post the new Post from the database
     */
    public static function createFromID($idPost,&$owner=null){
        $db = Database::iniStandard();
        $sql_selectPost = 'SELECT p.idPost,p.dtTitle,p.dtContent, fiBlog,tblUser.dtFirstName,tblUser.dtLastName
                           FROM tblPost AS p, tblBlog AS B, tblUser
                           WHERE idUser=B.fiUser AND B.idBlog=fiBlog AND idPost = :postID';
        $stm = $db->getConnection()->prepare($sql_selectPost);
        $stm->bindValue(':postID',$idPost,PDO::PARAM_INT);
        $stm->execute();
        $res = $stm->fetch(PDO::FETCH_BOTH);
        if(count($res) !=0){
            $owner = $res[3].' '.$res[4];
            return Post::createFromRes($res,true);
        }else{
            //No post found
            header("Location: ./index.php?msg=".err_postNotFound.'&class=alert-danger');
        }
    }

    /**
     * Add a new Post to the database
     * @param int $blogId The id to the blog
     * @param string $dtTitle Title of the blog
     * @param string $dtContent Content of the blog
     * @return Post The new Post
     */
    public static function createAndAddToDB($blogId,$dtTitle,$dtContent){
        $dtContentParsed = Post::parseText($dtContent);
        $sql_insPost = 'INSERT INTO tblPost(dtTitle,dtContent,fiBlog) VALUES (:title,:content,:fiBlog)';
        $db = Database::iniStandard();
        $stm = $db->getConnection()->prepare($sql_insPost);
        $stm->bindValue(':title',$dtTitle);
        $stm->bindValue(':content',$dtContentParsed);
        $stm->bindValue(':fiBlog',$blogId,PDO::PARAM_INT);
        $stm->execute();
        $postID= $db->getConnection()->lastInsertId();
        return new Post($postID,$dtTitle,$blogId,$dtContentParsed);
    }

    /**
     * Post constructor.
     * @param $idPost int|string id of the Post in Database
     * @param $dtTitle string Title of the Post
     * @param $blogID int | string ID of the containing blog
     * @param $dtContent string | null Content of the Post
     */
    public function __construct($idPost,$dtTitle,$blogID,$dtContent=null)
    {
        $this->idPost = $idPost;
        $this->dtTitle = $dtTitle;
        $this->dtContent = $dtContent;
        $this->blogId = $blogID;
        $db = Database::iniStandard();
        $sql_comment = "SELECT idComment,dtTitle,dtContent,fiUser FROM tblComment WHERE fiPost = :postid";
        $stm = $db->getConnection()->prepare($sql_comment);
        $stm->bindValue(':postid',$idPost,PDO::PARAM_INT);
        $stm->execute();
        $res = $stm->fetchAll(PDO::FETCH_ASSOC);
        foreach($res as $r){
            $this->comments[] = Comment::createFromRes($r);
        }
    }

    /**
     * Create html to display this post header
     * @return string Html with the post header
     */
    public function displayPostHeader(){
        return <<<EOTHML
<article class="panel panel-primary">
        <section class="panel-heading">
            <h2 class="panel-title">{$this->dtTitle}</h2>
        </section>
        <section class="panel-body">
            <p class="col-md-2">{$this->getCommentNumber()} Comments</p>
            <p class="col-md-8"></p>
            <p class="col-md-2">
                <a class="btn btn-lg btn-primary" href="index.php?action=post&id={$this->idPost}" role="button">
            Show <span class="glyphicon glyphicon-arrow-right"></span>
                </a>
            </p>
        </section>
    </article>
EOTHML;
    }

    /**
     * Gets the number of comments in this post
     * @return int Number of comments in this post
     */
    public function getCommentNumber(){
        return count($this->comments);
    }

    /**
     * Displays a post as Html
     * @return string Html of the post
     */
    public function toHtml(){
        if(isset($this->dtContent)){
            return $this->dtContent;
        }else{
            return '<div class="alert-danger alert"><span class="glyphicon glyphicon-alert"></span> 404 Content not Found</div>';
        }
    }

    /**
     * Parse some text and returns the result
     * @param $text string Text to be parsed
     * @return string result of the parser
     */
    private static function parseText($text){
        if(cfg_parser_useParsedown == 1){
            $parser = new Parsedown();
            return $parser->text($text);
        }else{
            return $text;
        }
    }

    /**
     * Get the title of the Post
     * @return string Title of the Post
     */
    public function getTitle()
    {
        return $this->dtTitle;
    }

    /**
     * Get the id of the Post
     * @return int|string
     */
    public function getIdPost()
    {
        return $this->idPost;
    }

    /**
     * Gets the Comments HTML
     */
    public function getComments(){
        $out = '';
        foreach($this->comments as $c){
            $out .= $c->toHtml();
        }
        return $out;
    }

    /**
     * Get the array with the Comments
     * @return array Comments array
     */
    public function getCommentsArray(){
        return $this->comments;
    }

    /**
     * Get the blog id
     * @return int|string th blog id
     */
    public function getBlogId()
    {
        return $this->blogId;
    }


    /**
     * Add a new Comment to this post and saves it in the database
     * @param string $title Title of the new Comment
     * @param string $content Content of the new Comment
     * #@param User $user The creator of the Comment
     */
    public function addComment($title,$content,$user){
        $this->comments[] = Comment::createAndAddToDB($title,$content,$this,$user);
    }

    /**Updates this Post local and in the Database
     * @param string | null $dtTitle The new Title of the post (If null field not changed)
     * @param string | null $dtContent The new Content of the post (If null field not changed)
     * @return bool Success
     */
    public function editAndUpdateDataBase($dtTitle,$dtContent){
        $query = '';
        $db = Database::iniStandard();
        if(isset($dtTitle)){
            $query .= 'dtTitle = '.$db->getConnection()->quote($dtTitle);
            $this->dtTitle=$dtTitle;
        }
        if(isset($dtContent)){
            $query .= ($query===''?'':', ').'dtContent = '.$db->getConnection()->quote($this->parseText($dtContent));
            $this->dtContent = $this->parseText($dtContent);
        }
        if($query !== ''){
            $query .= " WHERE idPost = ".$db->getConnection()->quote($this->idPost,PDO::PARAM_INT);
            $query = 'UPDATE tblPost SET '.$query;
            $db->getConnection()->exec($query);
            return true;
        }
        return false;
    }

    /**
     * Deletes the actual Comment
     * @return bool Success
     */
    public function deleteMe(){
        if(unserialize($_SESSION['user'])->isBackend()||$_SESSION['blogID']===$this->blogId){
            $db = Database::iniStandard();
            $db->getConnection()->query("DELETE FROM tblPost WHERE idPost={$this->idPost}");
            return true;
        }else{
            return false;
        }
    }
}