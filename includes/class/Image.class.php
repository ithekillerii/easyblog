<?php
/**
 * Image Class
 */
class Image {

    const UPLOAD_PATH = 'images/upload/';

    private $idImage;
    private $dtName;
    private $dtCaption;
    private $dtFilePath;
    private $dtTSCreation;
    private $user;

    /**
     * Creates a new Image and add It to the Database
     * @param string $dtName Name of the Image (user name)
     * @param string $dtCaption Caption of the Image
     * @param string $dtFilePath File path on the Server relative to uploads Folder
     * @param User $user The owner of the image
     * @return Image the new Image
     */
    public static function createAndAddToDB($dtName,$dtCaption,$dtFilePath,$user){
        $query = "INSERT INTO tblImage(dtName, dtCaption, dtFilePath, fiUser) VALUES (:imgName, :cap, :path, :fiUser)";
        $db = Database::iniStandard();
        $stm = $db->getConnection()->prepare($query);
        $stm->bindValue(':imgName',$dtName);
        $stm->bindValue(':cap',$dtCaption);
        $stm->bindValue(':path',$dtFilePath);
        $stm->bindValue(':fiUser',$user->getIdUser(),PDO::PARAM_INT);
        $stm->execute();
        return new Image($db->getConnection()->lastInsertId(),$dtName,$dtCaption,$dtFilePath,'',$user);
    }

    /**
     * Creates a new Image from an ID
     * @param int|string $id Id of the record in the Table
     * @return Image The Image from the Database
     */
    public static function createFromID($id){
        $query = "SELECT * FROM tblImage WHERE idImage = :id";
        $db = Database::iniStandard();
        $stm = $db->getConnection()->prepare($query);
        $stm->bindValue(':id',$id,PDO::PARAM_INT);
        $stm->execute();
        $res = $stm->fetchAll(PDO::FETCH_ASSOC);
        if(isset($res[0]['idImage'])){
            return Image::createFromRes($res[0]);
        }else{
            return null;
        }

    }

    /**
     * Creates a new Image from a Database result set
     * @param array $res Result set from pdo with FETCH_ASSOC
     * @return Image The new Image
     */
    public static function createFromRes($res){
        $user = User::createFromID($res['fiUser']);
        return new Image($res['idImage'],$res['dtName'],$res['dtCaption'],$res['dtFilePath'],$res['dtTSCreation'],$user);
    }

    /**
     * Image constructor.
     * @param string|int $idImage Id of the Image Record
     * @param string $dtName Name of the Image (user name)
     * @param string $dtCaption Caption of the Image
     * @param string $dtFilePath File path on the Server relative to uploads Folder
     * @param string $dtTSCreation
     * @param User $user
     */
    public function __construct($idImage, $dtName, $dtCaption, $dtFilePath, $dtTSCreation, $user)
    {
        $this->idImage = $idImage;
        $this->dtName = $dtName;
        $this->dtCaption = $dtCaption;
        $this->dtFilePath = $dtFilePath;
        $this->dtTSCreation = $dtTSCreation;
        $this->user = $user;
    }

    //##################################################################################################################

    /**
     * @return int|string
     */
    public function getIdImage()
    {
        return $this->idImage;
    }

    /**
     * @return string
     */
    public function getDtName()
    {
        return $this->dtName;
    }

    /**
     * @return string
     */
    public function getDtCaption()
    {
        return $this->dtCaption;
    }

    /**
     * @return string
     */
    public function getDtFilePath()
    {
        return $this->dtFilePath;
    }

    /**
     * @return string
     */
    public function getDtTSCreation()
    {
        return $this->dtTSCreation;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Detete this image
     * @param bool $deleteFromServer if set to true it also delete the file on the server else only in the database
     */
    public function deleteMe($deleteFromServer = true){
        $usr = unserialize($_SESSION['user']);
        if($usr->isBackend() || $usr->getIdUser() === $this->user->getIdUser()){
            $query = "DELETE FROM tblImage WHERE idImage = {$this->getIdImage()}";
            $db = Database::iniStandard();
            $db->getConnection()->query($query);
            if($deleteFromServer){
                unlink(basePath.self::UPLOAD_PATH.$this->getDtFilePath());
                //exec("rm $file");
            }
        }
    }

    /**
     * Updates this Object an the Database
     * @param string $caption The new Caption
     * @param string $name The new Name
     * @return bool success
     */
    public function updateMeAndDatabase($caption,$name){
        $query = '';
        $db = Database::iniStandard();
        if(isset($caption)){
            $query .= 'dtCaption = '.$db->getConnection()->quote($caption);
            $this->dtCaption=$caption;
        }
        if(isset($name)){
            $query .= ($query===''?'':', ').'dtName = '.$db->getConnection()->quote($name);
            $this->dtName = $this->$name;
        }
        if($query !== ''){
            $query .= " WHERE idImage = ".$db->getConnection()->quote($this->idImage,PDO::PARAM_INT);
            $query = 'UPDATE tblImage SET '.$query;
            $db->getConnection()->exec($query);
            return true;
        }
        return false;
    }
}