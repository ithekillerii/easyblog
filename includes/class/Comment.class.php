<?php

/**
 * Class Comment
 * Represent Comment Entry
 */
class Comment {

    private $changed = false;
    private $id;
    private $title;
    private $content;
    private $user;

    /**
     * Create a new Comment from Database with the id.
    * @param $idComment int | string idComment in database
    * @return Comment the new Comment from the database
    */
    public static function createFromID($idComment){
        $db = Database::iniStandard();
        $sql_selectPost = 'SELECT * FROM tblComment WHERE idComment = :commnetID';
        $stm = $db->getConnection()->prepare($sql_selectPost);
        $stm->bindValue(':commnetID',$idComment,PDO::PARAM_INT);
        $stm->execute();
        $res = $stm->fetch(PDO::FETCH_ASSOC);
        if(count($res) !=0){
            return Comment::createFromRes($res);
        }else{
            //No post found
            header("Location: ./index.php?msg=".err_postNotFound.'&class=alert-danger');
        }
    }

    /**
     * Create new Comment from Database Resource
     * @param $res array Database Recourse from PDO with FETCH_Assoc
     * @return Comment The created comment
     */
    public static function createFromRes($res){
        $db = Database::iniStandard();
        $sql_comment = "SELECT idUser,dtUsername,dtFirstName,dtLastName,dtTSLastLogin,dtLocked,dtBackend FROM tblUser WHERE idUser = :uid";
        $stm = $db->getConnection()->prepare($sql_comment);
        $stm->bindValue(':uid',$res['fiUser'],PDO::PARAM_INT);
        $stm->execute();
        $resUser = $stm->fetch(PDO::FETCH_ASSOC);
        return new Comment($res['idComment'],$res['dtTitle'],$res['dtContent'],User::createFromDBRes($resUser));
    }

    /**
     * Creates a new Comment and add it to the Database
     * @param $title String Title of the Comment
     * @param $content String Content of the Comment
     * @param $post Post The Post where the comment have to be added
     * @param $user User Creator of the Comment
     * @return Comment The new Comment
     */
    public static function createAndAddToDB($title,$content,$post,$user){
        $db = Database::iniStandard();
        $sql_insertComment = "INSERT INTO tblComment(dtTitle,dtContent,fiPost,fiUser) VALUES (:title,:content,:postID,:userID)";
        $stm = $db->getConnection()->prepare($sql_insertComment);
        $stm->bindValue(':title',htmlentities($title));
        $stm->bindValue(':content',htmlentities($content));
        $stm->bindValue(':postID',$post->getIdPost());
        $stm->bindValue(':userID',$user->getIdUser());
        $stm->execute();
        $commentID= $db->getConnection()->lastInsertId();
        return new Comment($commentID,$title,$content,$user);
    }

    /**
     * Comment constructor.
     * @param int $id ID of the Post in the Database
     * @param string $title Title of the Comment
     * @param string $content Content of the Comment
     * @param User $user Creator of the Comment
     */
    public function __construct($id, $title, $content, $user)
    {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->user = $user;
    }

    /**
     * Update Database
     * @deprecated
     */
    public function updateDB(){
        if($this->changed){

        }
    }

    /**
     * Returns a Comment as html
     * @return string Html of a comment
     */
    public function toHtml(){
        $from = $this->user->getDtFirstName().' '.$this->user->getDtLastName();
        $btn = '';
        if(isset($_SESSION['user'])){
            if($_SESSION['user'][0]===$this->user->getUsername()){
                $btn='<button id="addComment" class="btn btn-primary" href="" role="button">
                    Edit &nbsp;<span class="glyphicon glyphicon-pencil"></span>
                </button>';
            }
        }
        return <<<EOHTML
<article class="panel panel-primary panel-collapse">
        <section class="panel-heading">
            <h2 class="panel-title">$this->title</h2>
            <small>from $from</small>
        </section>
        <section class="panel-body">
            <p class="col-md-11">$this->content</p>
            <p class="col-md-1">
                $btn
            </p>
        </section>
    </article>
EOHTML;
    }

    /**
     * Get the Title of the comment
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the Content of the comment
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Get the User of the comment
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Updates this Comment local and in the Database
     * @param string | null $dtTitle The new Title of the Comment (If null field not changed)
     * @param string | null $dtContent The new Content of the Comment (If null field not changed)
     * @return bool Success
     */
    public function editAndUpdateDataBase($dtTitle,$dtContent){
        $query = '';
        $db = Database::iniStandard();
        if(isset($dtTitle)){
            $query .= 'dtTitle = '.$db->getConnection()->quote($dtTitle);
            $this->title=$dtTitle;
        }
        if(isset($dtContent)){
            $query .= ($query===''?'':', ').'dtContent = '.$db->getConnection()->quote($dtContent);
            $this->content = $dtContent;
        }
        if($query !== ''){
            $query .= " WHERE idComment = ".$db->getConnection()->quote($this->id,PDO::PARAM_INT);
            $query = 'UPDATE tblComment SET '.$query;
            $db->getConnection()->exec($query);
            return true;
        }
        return false;
    }

    /**
     * Deletes the actual Comment
     * @return bool Success
     */
    public function deleteMe(){
        if(unserialize($_SESSION['user'])->isBackend()||unserialize($_SESSION['user'])->getIdUser()===$this->user->getIdUser()){
            $db = Database::iniStandard();
            $db->getConnection()->query("DELETE FROM tblComment WHERE idComment={$this->id}");
            return true;
        }else{
            return false;
        }
    }

}