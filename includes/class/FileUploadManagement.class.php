<?php

/**
 * Class to manage the File Upload
 */
class FileUploadManagement
{

    /**
     * Regex to check if Path is valid.
     * This one is only allows Alphanumeric, _ , / and . (Recommended UNIX/LINUX and WINDOWS systems)
     */
    const REGEX_PATH = '/^[A-Za-z0-9_.\/]+$/';
    /**
     * The Separator for the path and File.
     * / Is for UNIX/LINUX system (but works on WINDOWS too) and \ is for WINDOWS systems (!! DONT WORK ON UNIX/LINUX !!)
     */
    const SEPARATOR = '/';

    //File Size Constants
    const B = 1;
    const KB = 1024;
    const MB = 1048576;
    //const GB = 1024 * 1024 * 1204;

    /**
     * @var int Maximum allowed size of the File
     */
    private $maxSize = 2097152;
    /**
     * @var array Array with the allowed File extensions (without the period)
     */
    private $WL_Extensions = array('csv', 'txt');
    /**
     * @var array Array with the allowed MimeTypes of the File
     */
    private $WL_MimeType = array('text/plain', 'text/csv');
    /**
     * @var string Name of the upload Field
     */
    private $uploadFieldName;

    /**
     * FileUploadManagement constructor.
     * @param string $uploadFieldName Name of the Upload Field
     */
    public function __construct($uploadFieldName)
    {
        $this->uploadFieldName = $uploadFieldName;
    }

    /**
     * Get the File extension of a file name
     * @return string Extension
     */
    public function getFileExt(){
        $fileName = $_FILES[$this->uploadFieldName]['name'];
        return substr($fileName,strpos($fileName,'.')+1);
    }

    /**
     * Moves the Uploaded File to the Server directory
     * @param $serverDir String Path on the server to save File
     * @param string $extension String Extension of the file (must be in WL_Extensions) and not contain a .
     * @param bool $serverSave User Server-safe name or not
     * @return array|null array('serverFile' => pathToFileOnServer, 'userFile'=>nameOfOriginalFile) or NULL if File Check, move, or Extension is Invalid
     */
    public function moveFile($serverDir, $extension = 'txt', $serverSave = false)
    {
        if ($this->isFileValid() && preg_match(self::REGEX_PATH, $serverDir) == 1) {
            $filePath = $serverDir.((substr($serverDir, -1) === '/')?'':'/');
            $fileName = ($serverSave ? $this->getServerSafeName($extension) : $_FILES[$this->uploadFieldName]['name']);
            $fileOnServer = $filePath.$fileName;
            if (move_uploaded_file($_FILES[$this->uploadFieldName]['tmp_name'], $fileOnServer)) {
                return array('serverFile' => $fileName, 'serverPath' => $filePath,'userFile' => $_FILES[$this->uploadFieldName]['name']);
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

    /**
     * Check if the File is valid or not
     * @return bool File valid or not
     */
    public function isFileValid()
    {
        if (isset($_FILES[$this->uploadFieldName])) {
            $fileExt = end(explode('.', $_FILES[$this->uploadFieldName]['name']));
            if ($_FILES[$this->uploadFieldName]['error'] == 0 && in_array($_FILES[$this->uploadFieldName]['type'], $this->WL_MimeType) && in_array($fileExt, $this->WL_Extensions) &&
                $_FILES[$this->uploadFieldName]['size'] < $this->maxSize
            ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get a Server-safe Name using the md5 of the Time + the original name of the File
     * @param string $extension Extension of the File
     * @return null|string Server-safe Name + extension
     */
    public function getServerSafeName($extension = 'txt')
    {
        if ($_FILES[$this->uploadFieldName]['error'] == 0) {
            if (in_array($extension, $this->WL_Extensions)) {
                return md5(time() . $_FILES[$this->uploadFieldName]['name']) . '.' . $extension;
            } else {
                return md5(time() . $_FILES[$this->uploadFieldName]['name']) . '.txt';
            }
        }
        return NULL;
    }

    /**
     * Set the Maximum size for the files
     * @param int $maxSize Maximum size for the files
     */
    public function setMaxSize($maxSize)
    {
        $this->maxSize = $maxSize;
    }

    /**
     * Set the allowed File Extension array
     * @param array $WL_Extensions Allowed File Extension array
     */
    public function setWLExtensions($WL_Extensions)
    {
        $this->WL_Extensions = $WL_Extensions;
    }

    /**
     * Set the allowed MimeType Array
     * @param array $WL_MimeType Allowed MimeType Array
     */
    public function setWLMimeType($WL_MimeType)
    {
        $this->WL_MimeType = $WL_MimeType;
    }

    /**
     * Set the name of the Upload Field
     * @param string $uploadFieldName Name of the Upload Field
     */
    public function setUploadFieldName($uploadFieldName)
    {
        $this->uploadFieldName = $uploadFieldName;
    }

    /**
     * Add one extension to the Whitelist of extensions
     * @param string $ext Extension without the period between filename and extension
     */
    public function addAllowedExtension($ext)
    {
        $this->WL_Extensions[] = $ext;
    }

    /**
     * Add one MimeType to the Whitelist of MimeTypes
     * @param string $mimeType MimeType to be added.
     */
    public function addAllowedMimeType($mimeType)
    {
        $this->WL_MimeType[] = $mimeType;
    }

}