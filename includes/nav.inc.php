<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">EasyBlog</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="<?= page_backend === 'true' ? 'backend.php' : 'index.php' ?>">Home</a></li>
                <?php
                if (isset($actualUser)) {
                if (page_backend === 'false') { ?>
                    <li><a href="?action=blog&id=<?= $_SESSION['blogID'] ?>">My Blog</a></li>
                    <li><a href="?action=settings">Settings</a></li>
                    <?php if ($actualUser->isBackend()) {
                        echo '<li><a href="backend.php">Backend</a></li>';
                    }
                } ?>
                <li><a>Welcome <?= $actualUser->getDtFirstName() . ', ' . $actualUser->getDtLastName() ?></a></li>
            </ul>
            <div class="navbar-right navbar-form">
                <a class="btn btn-danger"
                   href="<?= page_backend === 'true' ? 'backend.php' : 'index.php' ?>?action=logout">Logout</a>
            </div>
            <?php } else {
                genToken(frmFrontend);
                ?>
            </ul>
                <form action="" method="post" class="navbar-form navbar-right" name="<?= frmFrontend ?>">
                    <div class="form-group">
                        <input type="text" placeholder="Username" class="form-control" name="username">
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" class="form-control" name="password">
                    </div>
                    <input type="submit" class="btn btn-success" value="Login" name="<?= frmFrontend ?>_submit">
                    <input type="hidden" name="frontendLogin_token" value="<?= getToken(frmFrontend) ?>">
                </form>
            <?php } ?>
        </div>
    </div>
</nav>