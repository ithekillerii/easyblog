<?php
/**
 * Basic include File for the Project
 */
//######################### Constants #########################

define('basePath', './'); //Relative path from script to the root of the Page

//Error messages:
define('err_blogNotFound',htmlspecialchars('404 Blog not found ¯\_(ツ)_/¯'));
define('err_postNotFound',htmlspecialchars('404 Post not found ¯\_(ツ)_/¯'));
define('err_userNotFound',htmlspecialchars('404 User not found ¯\_(ツ)_/¯'));

//######################### Invoke classLoader #########################
require_once('includes/classLoader.inc.php');

//######################### Login management #########################
session_start();
session_regenerate_id(true);
if (isset($_SESSION['user'])) {
    $actualUser = unserialize($_SESSION['user']);
} else {
    $actualUser = login();
}
//#########################  Action Management (ROUTER)#########################
$page = new IndexPage($actualUser); //Fallback to the index Page if action is invalid
if (page_backend === 'true') {
    $page = new DashBoardPage($actualUser, 0); //Fallback to the backend Page if action is invalid
    if (isset($_GET['action'])) {
        require_once(basePath . 'includes/router_backend.inc.php');
    }
} else {
    $page = new IndexPage($actualUser); //Fallback to the index Page if action is invalid
    if (isset($_GET['action'])) {
        require_once(basePath . 'includes/router_frontend.inc.php');
    }
}

//######################### Functions #########################
/**
 * Check if user has given the right credentials
 * @return NULL | array Returns the name of the user logged in
 */
function login()
{
    if (isset($_POST[frmFrontend . '_submit'])) {
        $in_pw = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
        if (checkToken($_POST[frmFrontend . '_token'], frmFrontend) AND isset($in_pw)) {

            //Query Database and look if password is correct
            $sql = "SELECT * FROM tblUser WHERE dtUsername = :user";
            $db = new Database(db_host, db_user, db_password, db_database);
            $stm = $db->getConnection()->prepare($sql);
            $stm->bindValue(':user', $_POST['username']);
            $stm->execute();
            $res = $stm->fetchAll(PDO::FETCH_ASSOC);
            if (isset($res[0])) {
                if (sha1($res[0]['dtSalt'] . $in_pw) === $res[0]['dtPassword'] && $res[0]['dtLocked'] === 'false') {
                    $username_safe = $db->getConnection()->quote($_POST['username']);
                    $updateSql = "UPDATE tblUser SET dtTSLastLogin = NOW() WHERE dtUsername = $username_safe";
                    $db->getConnection()->exec($updateSql);
                    $user = User::createFromDBRes($res[0]);
                } else {
                    $user = null;
                    $_GET['msg'] = htmlspecialchars('Wrong password');
                    $_GET['class'] = 'alert-warning';
                }
            } else {
                $_GET['msg'] = htmlspecialchars('Username not valid');
                $_GET['class'] = 'alert-warning';
                $user = null;
            }
            //Set uid to Session and
            if (isset($user)) {
                $sql_getBlog = "SELECT idBlog FROM tblBlog WHERE fiUser = {$user->getIdUser()}";
                $resBlog = $db->getConnection()->query($sql_getBlog)->fetchAll(PDO::FETCH_ASSOC);
                $ret = serialize($user);
                $_SESSION['user'] = $ret;
                $_SESSION['blogID'] = $resBlog[0]['idBlog'];
                return $user;
            }
        }
    }
    return null;
}

/**
 * Logs the User out and destroy the session
 * @param $action string action to be added to the query
 * @param $id string | int id to be added to the query
 */
function logout($action, $id)
{
    destroySession();
    if (isset($action)) {
        header("Location: ./index.php?action=" . $action . '&id=' . $id);
    } else {
        header("Location: ./index.php");
    }
}
