<ul class="nav nav-tabs nav-justified" id="settingsTabs">
    <li role="presentation" class="active"><a href="#" data-ref="personalInfo">Personal Informations</a></li>
    <li role="presentation"><a href="#" data-ref="blogInfo">Blog Informations</a></li>
    <li role="presentation"><a href="#" data-ref="postsInfo">Posts</a></li>
    <li role="presentation"><a href="#" data-ref="commentsInfo">Comments</a></li>
    <li role="presentation"><a href="#" data-ref="imagesInfo">My Images</a></li>
</ul>
<div class="tabBody" id="personalInfo">
    <form method="post" action="" class="form-horizontal">
        <h2>Login</h2>

        <div class="form-group">
            <label for="inputEmail" class="col-sm-2 control-label">Change Email</label>

            <div class="col-sm-10">
                <input type="email" class="form-control" id="inputEmail" name="settings_email" placeholder="Email">
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword" class="col-sm-2 control-label">Change Password</label>

            <div class="col-sm-10">
                <input type="password" class="form-control" name="settings_password" id="inputPassword"
                       placeholder="Password">
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword2" class="col-sm-2 control-label">Retype Password</label>

            <div class="col-sm-10">
                <input type="password" class="form-control" name="settings_password_retype" id="inputPassword2"
                       placeholder=" Retype Password">
            </div>
        </div>
        <h2>Personal Informations</h2>

        <div class="form-group">
            <label for="inputFname" class="col-sm-2 control-label">Change Firstname</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputFname" name="settings_firstname"
                       placeholder="Firstname">
            </div>
        </div>
        <div class="form-group">
            <label for="inputLname" class="col-sm-2 control-label">Change Last</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputLname" name="settings_lastname"
                       placeholder="Lastname">
            </div>
        </div>
        <h2>Unregister</h2>

        <div class="form-group">
            <a id="unregister" href="index.php?action=unregister" class="btn btn-sm btn-info">Unregister now</a>
        </div>
        <br><br>

        <div class="form-group">
            <input type="submit" id="save-personal" class="btn btn-lg btn-success" name="settings_submit_personal" value="Submit">
            <a id="reject" href="" class="btn btn-lg btn-warning">Reject</a>
            <input type="hidden" value="<?=getToken($frmPersonal)?>" name="settings_personal_token">
        </div>
    </form>
</div>
<div class="tabBody" id="blogInfo">
    <form method="post" action="" class="form-horizontal">
        <h2>Edit Blog</h2>
        <div class="form-group">
            <label for="inputBlogName" class="col-sm-2 control-label">Change Title</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputBlogName" name="settings_blogName" placeholder="Blog name">
            </div>
        </div>
        <div class="form-group">
            <label for="inputBlogDesc" class="col-sm-2 control-label">Change Blog Description</label>
            <div class="col-sm-10">
                <textarea rows="10" class="form-control" id="inputBlogName" name="settings_blogDesc" placeholder="Blog Description"></textarea>
            </div>
        </div>
        <div class="form-group">
            <input type="submit" id="save-blog" class="btn btn-lg btn-success" name="settings_submit_blog" value="Submit">
            <a id="reject" href="" class="btn btn-lg btn-warning">Reject</a>
            <input type="hidden" value="<?=getToken($frmBlog)?>" name="settings_blog_token">
        </div>
    </form>
</div>
<div class="tabBody" id="postsInfo" data-type="post"></div>
<div class="tabBody" id="commentsInfo" data-type="comment"></div>
<div class="tabBody" id="imagesInfo" data-type="image"></div>