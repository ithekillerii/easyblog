<?php
/**
 * Frontend Router
 */
switch($_GET['action']){
    case 'blog':
        if(isset($_GET['id'])){
            $page = new BlogPage($_GET['id'],$actualUser);
        }else{
            $page->addMessage('404 Page not Found','alert-danger');
        }
        break;
    case 'post':
        if(isset($_GET['id'])){
            $page = new PostPage($actualUser);
        }else{
            $page->addMessage('404 Page not Found','alert-danger');
        }
        break;
    case 'settings':
        if(isset($actualUser)){
            $page = new SettingsPage($actualUser);
        }else{
            $page->addMessage('Please Login','alert-warning');
        }
        break;
    case 'register':
        $page = new RegisterPage($actualUser);
        break;
    case 'unregister':
        $page = new IndexPage($actualUser,true);
        break;
    case 'logout':
        logout(null,0);
        break;
    default:
        $page = new IndexPage($actualUser);
}