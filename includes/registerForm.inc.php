<form class="form-horizontal" action="" method="post">
    <p>Login informations</p>
    <div class="form-group">
        <label class="col-sm-2">Username</label>
        <div class="col-sm-5"><input class="form-control" type="text" id="inUsername" name="reg_username" <?=isset($this->user)?'value="'.$this->user.'"':'placeholder="Username"'?>></div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Password</label>
        <div class="col-sm-5"><input class="form-control" type="password" id="inPassword" name="reg_Password" placeholder="Password"></div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Retype Password</label>
        <div class="col-sm-5"><input class="form-control" type="password" id="inPassword_rep" name="reg_Password-rep" placeholder="Repeat Password"></div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Email</label>
        <div class="col-sm-5"><input class="form-control" type="email" id="inEmail" name="reg_Email" <?=isset($this->user)?'value="'.$this->email.'"':'placeholder="Email"'?>></div>
    </div>
    <p>Personal Information</p>
    <div class="form-group">
        <label class="col-sm-2">First Name</label>
        <div class="col-sm-5"><input class="form-control" type="text" id="inFirstName" name="reg_firstName" <?=isset($this->user)?'value="'.$this->firstName.'"':'placeholder="First Name"'?>></div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Last Name</label>
        <div class="col-sm-5"><input class="form-control" type="text" id="inLastName" name="reg_lastName" <?=isset($this->user)?'value="'.$this->lastName.'"':'placeholder="Last Name"'?>></div>
    </div>
    <p>Blog Informations (can be changed later)</p>
    <div class="form-group">
        <label class="col-sm-2">Blog Name</label>
        <div class="col-sm-5"><input class="form-control" type="text" id="inBlogTitle" name="reg_blogTitle" <?=isset($this->user)?'value="'.$this->blogName.'"':'placeholder="Blog Title"'?>></div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Blog Description</label>
        <div class="col-sm-5">
            <textarea class="form-control" id="inBlogDesc" name="reg_blogDesc" rows="5" <?=isset($this->blogDesc)?'>'.$this->blogDesc:'placeholder="Blog Description" >'?></textarea>
        </div>
    </div>
    <small>Max 120 Characters</small>
    <input type="hidden" name="reg_token" value="<?=getToken('frm_register')?>">
    <div class="form-group">
        <img id="captcha" src="<?=basePath?>includes/securimage/securimage_show.php" alt="CAPTCHA Image" >
        <input type="text" name="captcha_code" >
        <a href="#" onclick="document.getElementById('captcha').src = 'includes/securimage/securimage_show.php?' + Math.random(); return false"><span class="glyphicon glyphicon-refresh"></span> New CAPTCHA</a>

    </div>
    <br>
    <div class="form-group">
        <div class="col-sm-2">
            <input class="btn btn-primary" type="submit" name="reg_submit" value="Register">
        </div>
    </div>
</form>