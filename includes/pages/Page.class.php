<?php

/**
 * Page abstract class
 * Abstract Controller Class for the Pages
 */
abstract class Page {

    protected $actualUser;
    protected $msgArray = array();


    /**
     * Page constructor.
     * @param $actualUser User The actual user logged in
     * @param $useGETMessage bool Display the GET Message
     */
    public function __construct($actualUser,$useGETMessage = true)
    {
        $this->actualUser = $actualUser;
        if($useGETMessage)$this->loadGetMSG();
    }

    /**
     * Print the header of the Page
     */
    public abstract function printHeader();

    /**
     * Print the body of the Page
     */
    public abstract function printBody();

    /**
     * Print the Messages in the message array
     */
    protected function printHeadMSG(){
        foreach($this->msgArray AS $msg){
            echo $msg."\n";
        }
    }

    /**
     * Load the getMessage in the message array
     * Uses $_GET['class'] for the bootstrap message class
     * Uses $_GET['msg'] for the message text.
     */
    public function loadGetMSG(){
        if(isset($_GET['msg'],$_GET['class'])){
            $this->msgArray[]='<div class="alert '.$_GET['class'].'">'.$_GET['msg'].'</div>';
        }
    }

    /**
     * Load the getMessage in the message array
     * @param string $msg for the message text. for the bootstrap message class
     * @param string $msgClass for the bootstrap message class
     */
    public function addMessage($msg, $msgClass){
        $this->msgArray[]='<div class="alert '.$msgClass.'">'.$msg.'</div>';
    }

    /**
     * Print the javascript behind the Jquery.
     */
    public function printScript(){
        echo '<script src="js/main.js"></script>';
    }

    /**
     * Print out the header to load Bootstrap
     */
    public final function inc_bs_Head(){
  echo '<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/myStyle.css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->';
    }

    /**
     * Print out the footer to load Bootstrap and JQuery
     */
    public final function inc_bs_Foot(){
        echo '<script src="js/jquery-2.1.4.min.js"></script>
              <script src="bootstrap/js/bootstrap.min.js"></script>';
    }
}