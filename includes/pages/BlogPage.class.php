<?php

/**
 * Class BlogPage
 * Controller Class of the Blog Page
 */
class BlogPage extends Page{

    private $blog;

    /**
     * BlogPage constructor.
     * @param $actualUser User The Actual User
     * @param $id int | string The id of the blog
     */
    public function __construct($id,$actualUser)
    {
        parent::__construct($actualUser);
        $this->blog = Blog::createFromID($id);
    }

    /**
     * Print the header of the Page
     */
    public function printHeader(){
        $postTxt = $this->blog->getPostsNumber()==1?'Post':'Posts';
        $commentTxt = $this->blog->getCommentsNumber()==1?'Comment':'Comments';
        echo "<h2>{$this->blog->getTitle()}</h2>";
        echo "<p>{$this->blog->getDesc()}</p>";
        echo '<div class="row">';
        echo "<div class=\"col-md-2\">{$this->blog->getPostsNumber()} $postTxt</div>";
        echo "<div class=\"col-md-2\">{$this->blog->getCommentsNumber()} $commentTxt</div>";
        echo '<div class="col-md-6"></div>';
        if (!isset($this->actualUser)) {
            echo '<p><a class="btn btn-lg btn-primary col-md-2" href="index.php?action=register" role="button">Register now</a></p>';
        }else if($_SESSION['blogID'] == $_GET['id']){
            echo '<div><button id = "addPost" class="btn btn-lg btn-primary col-md-2" role="button"><span class="glyphicon glyphicon-plus"></span> Add Post</button></div>';
        }
        echo '</div>';
        echo "<div>from {$this->blog->getUser()->getDtFirstName()}, {$this->blog->getUser()->getDtLastName()}</div>";
        $this->printHeadMSG();
    }

    /**
     * Print the body of the Page
     */
    public function printBody(){
        echo $this->blog->displayPosts();
    }

    /**
     * Add a new Post to this blog from an ajax request
     * @param string $dtTitle Title of the new post
     * @param string $dtContent Content of the new Post
     * @return int ErrorCode 0 = no error 1 = not allowed
     */
    public function ajax_addPost($dtTitle,$dtContent){
        if($this->blog->autorized($this->actualUser->getIdUser())){
            $post = Post::createAndAddToDB($this->blog->getId(),$dtTitle,$dtContent);
            $this->blog->addPost($post);
            return 0;
        }else{
            return 1;
        }
    }
}