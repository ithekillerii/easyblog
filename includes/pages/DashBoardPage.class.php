<?php
/**
 * The Dashboard Page of the Backend
 */

class DashBoardPage extends BackendPage{

    /**
     * DashBoardPage constructor.
     * @param User $user ActualUser
     * @param int $pid Pageid for page Highlight
     */
    public function __construct($user,$pid)
    {
        parent::__construct($user,$pid);
    }

    /**
     * Print the body of the Page
     */
    public function printBody()
    {
        echo '<h2 class="sub-header">Dashboard - Actual Stats</h2>';
        echo $this->getStatTable();
    }

    /**Gets the Statistics Table
     * @return string HTML Table with stats.
     */
    private function getStatTable(){
        $db = Database::iniStandard();
        $hPost=$db->getRecordWithHighestField('tblPost','idPost');
        $hComment=$db->getRecordWithHighestField('tblComment','idComment');
        $hPostID = $hPost['idPost'];
        $hCommentID = $hComment['idComment'];
        return '<table class="table table-responsive"><tbody>'.
            "<tr><td>Registered Users</td><td>{$db->countRecords('tblUser')}</td></tr>".
            "<tr><td>Number of Posts</td><td>{$db->countRecords('tblPost')}</td></tr>".
            "<tr><td>Number of Comments</td><td>{$db->countRecords('tblComment')}</td></tr>".
            "<tr><td>Last Registered User</td><td>{$db->getRecordWithHighestField('tblUser','idUser')['dtUsername']}</td></tr>".
            "<tr><td>Last Post</td><td><a class=\"show\" id=\"highestPost\" data-id=\"$hPostID\" data-type=\"post\"><!--href=\"index.php?action=post&id=$hPostID\" -->{$hPost['dtTitle']}</a></td></tr>".
            "<tr><td>Last Comment</td><td><a class=\"show\" id=\"highestComment\" data-type=\"comment\" data-id=\"$hCommentID\"><!--href=\"index.php?action=comment&id=$hCommentID\" -->{$hComment['dtTitle']}</a></td></tr>".
            "</tbody></table>";

    }

    /**
     * Print the Javascript of the page
     */
    public function printScript()
    {
        parent::printScript();
        ?>
        <script>
            var reqUrl = '';

            $(document).on("click",'.closePopup',function(evt){
                evt.preventDefault();
                $("#ajax").hide();
                $("#ajax").text('');
            });

            $('.show').click(function(evt){
                evt.preventDefault();
                var id = $(this).attr('data-id');
                var type = $(this).attr('data-type');
                reqUrl="ajax/dynEdit.php?crud=show&type="+type+"&cid="+id;
                $('#ajax').load(reqUrl).show();
            });
        </script>
        <?php
    }


}