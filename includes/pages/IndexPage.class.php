<?php
/**
 * Class IndexPage
 * Controller Page of the index
 */
class IndexPage extends Page{

    private $blogList = array();
    private $unregister = false;

    /**
     * IndexPage Constructor
     * @param User|null $actualUser The actual User
     * @param $unregister bool Unregister User
     */
   function __construct($actualUser = null,$unregister = false){
       parent::__construct($actualUser);
       $db = new Database(db_host,db_user,db_password,db_database);
       $sql_blog = "SELECT b.idBlog,b.dtTitle,b.dtDescription,u.dtFirstName,u.dtLastName,count(fiBlog) AS postNum
                    FROM tblUser AS u, tblBlog AS b LEFT JOIN tblPost AS p
                    ON b.idBlog = p.fiBlog
                    WHERE b.fiUser=u.idUser
                    GROUP BY idBlog ";
       $stm = $db->getConnection()->query($sql_blog);
       $this ->blogList = $stm->fetchAll(PDO::FETCH_ASSOC);
       $this->unregister = $unregister;
   }

    /**
     * Print the Body
     */
    public function printBody(){
        $out = "";
        foreach($this->blogList as $blog){
            $postCount = $blog['postNum']==1?$blog['postNum'].' post':$blog['postNum'].' posts';
            $out .= <<< EOTHML
<article class="panel panel-primary">
        <section class="panel-heading">
            <h2 class="panel-title">{$blog['dtTitle']}</h2>
        </section>
        <section class="panel-body">
            <p class="description">{$blog['dtDescription']}</p>

            <p class="col-md-1">$postCount</p>
            <p class="col-md-9">By {$blog['dtFirstName']}, {$blog['dtLastName']}</p>

            <p class="col-md-2">
                <a class="btn btn-lg btn-primary" href="index.php?action=blog&id={$blog['idBlog']}" role="button">
                Visit Blog <span class="glyphicon glyphicon-arrow-right"></span>
                </a>
            </p>
        </section>
    </article>
EOTHML;
        }
        echo $out;
        if($this->unregister){
            if(isset($_POST['submit_popup'])&&checkToken($_POST['token_popup'],'frm_confirm')){
                $user = unserialize($_SESSION['user']);
                $user->unregister(false);
                logout(null,-1);
            }else{
                genToken('frm_confirm');
                Dialog::printConfirmDialog(0,'Confirm Unregister','Do you really want to Unregister?','frm_confirm',true,'index.php');
            }
        }
    }

    /**
     * Print the page Header
     */
    public function printHeader(){
        echo '<h1>Welcome to the EasyBlog</h1>
              <p>This is a light blogging software</p>';
        if (!isset($this->actualUser)) {
            echo '<p><a class="btn btn-lg btn-primary" href="index.php?action=register" role="button">Register now</a></p>';
        }
        $this->printHeadMSG();
    }
}