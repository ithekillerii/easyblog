<?php
/**
 * Created by IntelliJ IDEA.
 * User: chris
 * Date: 14.06.15
 * Time: 14:59
 */

class SettingsPage extends Page{

    /**
     * SettingsPage constructor.
     * @param User $actualUser The actual User
     */
    public function __construct($actualUser)
    {
        parent::__construct($actualUser);
        $this->evaluateForms();
    }

    /**
     * Print the header of the Page
     */
    public function printHeader()
    {
        echo '<h1>Settings</h1>';
        $this->printHeadMSG();
    }

    /**
     * Print the body of the Page
     */
    public function printBody()
    {
        $frmPersonal = 'editPersonal';
        $frmBlog = 'editBlog';
        genToken($frmPersonal);
        genToken($frmBlog);
        include_once('includes/settingsTabs.inc.php');
    }

    /**
     * Also add the tabs.js
     */
    public function printScript()
    {
        parent::printScript();
        echo '<script src="js/jquery.tablesorter.min.js"></script>';
        echo '<script src="js/tabs.js"></script>';
    }

    /**
     * Evaluate the incoming edit Forms
     */
    private function evaluateForms(){
        if(isset($_POST['settings_submit_personal'])&&checkToken($_POST['settings_personal_token'],'editPersonal')){
            $in_mail = filter_input(INPUT_POST,'settings_email',FILTER_SANITIZE_STRING,FILTER_FLAG_EMPTY_STRING_NULL);
            $in_pass1 = filter_input(INPUT_POST,'settings_password',FILTER_SANITIZE_STRING,FILTER_FLAG_EMPTY_STRING_NULL);
            $in_pass2 = filter_input(INPUT_POST,'settings_password_retype',FILTER_SANITIZE_STRING,FILTER_FLAG_EMPTY_STRING_NULL);
            $in_fName = filter_input(INPUT_POST,'settings_firstname',FILTER_SANITIZE_STRING,FILTER_FLAG_EMPTY_STRING_NULL);
            $in_lName = filter_input(INPUT_POST,'settings_lastname',FILTER_SANITIZE_STRING,FILTER_FLAG_EMPTY_STRING_NULL);

            $pass = null;

            if(isset($in_pass1,$in_pass2)) {
                if ($in_pass1 === $in_pass2) {
                    $pass = $in_pass1;
                }
            }

            $user = unserialize($_SESSION['user']);

            if($user->editAndUpdateDataBase(null,$pass,$in_mail,$in_fName,$in_lName,null,null)){
                $this->addMessage('Values Changed','alert-success');
            }else{
                $this->addMessage('Nothing Changed','alert-info');
            }
        }else if(isset($_POST['settings_submit_blog'])&&checkToken($_POST['settings_blog_token'],'editBlog')){
            $query = '';
            $db = Database::iniStandard();
            $in_title = filter_input(INPUT_POST,'settings_blogName',FILTER_SANITIZE_STRING,FILTER_FLAG_EMPTY_STRING_NULL);
            $in_dec = filter_input(INPUT_POST,'settings_blogDesc',FILTER_SANITIZE_STRING,FILTER_FLAG_EMPTY_STRING_NULL);
            if(isset($in_title)){
                $query .= 'dtTitle = '.$db->getConnection()->quote($in_title);
            }
            if(isset($in_dec)){
                $query .= ($query===''?'':', ').'dtDescription = '.$db->getConnection()->quote($in_dec);
            }
            if($query !== ''){
                $query .= " WHERE fiUser = ".unserialize($_SESSION['user'])->getIdUser();
                $query = 'UPDATE tblBlog SET '.$query;
                $db->getConnection()->exec($query);
            }else{
                $this->addMessage('Nothing Changed','alert-info');
            }
        }
    }

}