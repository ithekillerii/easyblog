<?php

/**
 * Class BackendPage
 * Blueprint for BackendPage
 */
abstract class BackendPage extends Page{

    private $pid;

    /**
     * BackendPage constructor.
     * @param User $user ActualUser
     * @param int $pid Pageid for page Highlight
     */
    public function __construct($user,$pid=0)
    {
        parent::__construct($user);
        $this->pid=$pid;
    }

    /**
     * Print the header of the Page
     */
    public function printHeader()
    {
        ?>
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li class="<?=$this->pid==0?'active':''?>"><a href="backend.php">Dashboard <?=$this->pid==0?'<span class="sr-only">(current)</span>':''?></a></li>
                <li class="<?=$this->pid==1?'active':''?>"><a>Manage</a></li>
                <li class="sub <?=$this->pid==2?'active':''?>"><a href="?action=users">Users <?=$this->pid==2?'<span class="sr-only">(current)</span>':''?></a></li>
                <li class="sub <?=$this->pid==3?'active':''?>"><a href="?action=blogs">Blogs <?=$this->pid==3?'<span class="sr-only">(current)</span>':''?></a></li>
                <li class="sub <?=$this->pid==4?'active':''?>"><a href="?action=posts">Post <?=$this->pid==4?'<span class="sr-only">(current)</span>':''?></a></li>
                <li class="sub <?=$this->pid==5?'active':''?>"><a href="?action=comments">Comments <?=$this->pid==5?'<span class="sr-only">(current)</span>':''?></a></li>
                <li class="sub <?=$this->pid==6?'active':''?>"><a href="?action=images">Images <?=$this->pid==6?'<span class="sr-only">(current)</span>':''?></a></li>
            </ul>
        </div>
        <?php
        // NOT USED NAV Points
        /*
         <li class="sub <?=$this->pid==7?'active':''?>"><a href="?action=stats">Statistics <?=$this->pid==7?'<span class="sr-only">(current)</span>':''?></a></li>
         */
    }
}