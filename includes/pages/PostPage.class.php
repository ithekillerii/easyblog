<?php
/**
 * Class PostPage
 * Post Page Controller Page
 */
class PostPage extends Page{

    private $post;
    private $from;

    /**
     * PostPage constructor.
     * @param User|null $actualUser The actual User
     */
    public function __construct($actualUser)
    {
        parent::__construct($actualUser);
        $user = '';
        $this->post=Post::createFromID($_GET['id'],$user);
        $this->from = $user;
    }

    /**
     * Print the header of the Page
     */
    public function printHeader()
    {
        echo "<h2>{$this->post->getTitle()}</h2>";
        echo "<p>{$this->post->getCommentNumber()} Comments</p>";
        echo '<div class="row">';
        echo "<small class=\"col-md-2\">from {$this->from}</small>";
        echo '<div class="col-md-8"></div>';
        if (!isset($this->actualUser)) {
            echo '<p><a class="btn btn-lg btn-primary col-md-2" href="index.php?action=register" role="button">Register now</a></p>';
        }else{
            echo '<div><button id = "addComment" class="btn btn-lg btn-primary col-md-2" href="" role="button"><span class="glyphicon glyphicon-plus"></span> Add Comment</button></div>';
            echo '<div><a id = "exportPDF" col-md-2" href="export.php?crud=pdf&type=post&cid='.$this->post->getIdPost().'" role="button"><span class="glyphicon glyphicon-download-alt"></span> Export as PDF</a></div>';
        }
        echo '</div>';
        $this->printHeadMSG();
    }

    /**
     * Print the body of the Page
     */
    public function printBody()
    {
        echo $this->post->toHtml();
        echo '<p class="separator"></p>';
        echo $this->post->getComments();
    }

    /**
     * Add a new Comment to this Post from an ajax request
     * @param string $title Title of the new Comment
     * @param string $content Content of the new Comment
     */
    public function ajax_addComment($title,$content){
        $user = unserialize($_SESSION['user']);
        $this->post->addComment($title,$content,$user);
    }

}