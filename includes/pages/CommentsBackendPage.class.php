<?php
/**
 * Backend Comment Page
 */

class CommentsBackendPage extends BackendPage{

    /**
     * CommentsBackendPage constructor.
     * @param User $user ActualUser
     * @param int $pid Pageid for page Highlight
     */
    public function __construct($user,$pid)
    {
        parent::__construct($user,$pid);
    }

    /**
     * Print the body of the Page
     */
    public function printBody()
    {
        echo '<h2 class="sub-header">Dashboard - Manage Comments</h2>';
        echo '<div data-type="comment">';
        echo '<input id="searchInput" placeholder="Type To Filter">';
        Dialog::printCrudTable('tblComment',false,true);
        echo '<button class="crudAdd btn btn-primary btn-lg"><span class="glyphicon glyphicon-plus"></span> Add Comment</button>';
        echo '</div>';
    }

    /**
     * Print the Javascript of the page
     */
    public function printScript()
    {
        parent::printScript();
        echo '<script>
                tab="#pageBody";
                pageUrl="backend.php?bodyOnly=0&action=comments";
              </script>';
        echo '<script src="js/tableFilter.js"></script>';
        echo '<script src="ajax/crud.js"></script>';
        echo '<script src="js/jquery.tablesorter.min.js"></script>';
        echo '<script>$("table").tablesorter();</script>';
    }


}