<?php
/**
 * Backend User Page
 */

class UsersBackendPage extends BackendPage{

    /**
     * UsersBackendPage constructor.
     * @param User $user ActualUser
     * @param int $pid Pageid for page Highlight
     */
    public function __construct($user,$pid)
    {
        parent::__construct($user,$pid);
    }

    /**
     * Print the body of the Page
     */
    public function printBody()
    {
        echo '<h2 class="sub-header">Dashboard - Manage Users</h2>';
        echo '<div data-type="user">';
        echo '<input id="searchInput" placeholder="Type To Filter">';
        Dialog::printCrudTable('tblUser',false,true);
        echo '<button class="crudAdd btn btn-primary btn-lg"><span class="glyphicon glyphicon-plus"></span> Add User</button>';
        echo ' <a class="crudCSV btn btn-warning btn-lg" data-id="-1"><span class="glyphicon glyphicon-download-alt"></span> Export as CSV</a>';
        echo '</div>';
    }
    /**
     * Print the Javascript of the page
     */
    public function printScript()
    {
        parent::printScript();
        echo '<script>
                tab="#pageBody";
                pageUrl="backend.php?bodyOnly=0&action=users";
              </script>';
        echo '<script src="js/tableFilter.js"></script>';
        echo '<script src="ajax/crud.js"></script>';
        echo '<script src="js/jquery.tablesorter.min.js"></script>';
        echo '<script>$("table").tablesorter();</script>';
    }
}