<?php

/**
 * Class RegisterPage
 * Controller Register Page
 */
class RegisterPage extends Page
{

    private $user = null;
    private $email = null;
    private $firstName = null;
    private $lastName = null;
    private $blogName = null;
    private $blogDesc = null;


    /**
     * RegisterPage constructor.
     * @param User|null $actualUser The actual User
     */
    public function __construct($actualUser)
    {
        parent::__construct($actualUser);
        require_once(basePath . 'includes/securimage/securimage.php');
        $this->register();
    }

    /**
     * Print the header of the Page
     */
    public function printHeader()
    {
        echo '<h1>Register to the EasyBlog</h1>';
    }

    /**
     * Print the body of the Page
     */
    public function printBody()
    {
        $this->printHeadMSG();
        include_once('includes/registerForm.inc.php');
    }

    /**
     * Register a new User
     * <ol>
     * <li>Check if token havn't been modified</li>
     * <li>Sanetize Inputs</li>
     * <li>Check if Inputvalues are valid and set</li>
     * <li>Inserts a new User and Blog into the Database</li>
     * </ol>
     */
    private function register()
    {
        if (isset($_POST['reg_submit'])) {
            //Token
            if (checkToken($_POST['reg_token'], 'frm_register')) {
                //Check Inputs
                $this->user = filter_input(INPUT_POST, 'reg_username', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                $pass = filter_input(INPUT_POST, 'reg_Password', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                $in_pwrep = filter_input(INPUT_POST, 'reg_Password-rep', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                $this->email = filter_input(INPUT_POST, 'reg_Email', FILTER_VALIDATE_EMAIL, FILTER_FLAG_EMPTY_STRING_NULL);
                $this->firstName = filter_input(INPUT_POST, 'reg_firstName', FILTER_SANITIZE_STRING);
                $this->lastName = filter_input(INPUT_POST, 'reg_lastName', FILTER_SANITIZE_STRING);
                $this->blogName = filter_input(INPUT_POST, 'reg_blogTitle', FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
                $this->blogDesc = filter_input(INPUT_POST, 'reg_blogDesc', FILTER_SANITIZE_STRING);
                //END
                //Check Values

                if (isset($this->user)) {
                    if (isset($this->email)) {
                        $checkres = User::check($this->user, $this->email);
                        switch ($checkres) {
                            case 0:
                                if (isset($pass, $in_pwrep)) {
                                    if ($pass === $in_pwrep) {
                                        $salt = User::genSalt();
                                        $encpw = User::hashPassword($pass, $salt);
                                        //Insert into Database
                                        if (isset($this->blogName)) {
                                            $securimage = new Securimage();
                                            if (!($securimage->check($_POST['captcha_code']) == false)) {

                                                User::addToDB($this->user, $encpw, $salt, $this->email, $this->firstName, $this->lastName, $this->blogName, $this->blogDesc);

                                                $msgFront = htmlspecialchars('Register Successful');
                                                header("Location: ./index.php?msg=" . $msgFront . '&class=alert-success');
                                            }else{
                                                $this->addMessage('CAPTCHA Invalid', 'alert-danger');
                                            }
                                        } else {
                                            $this->addMessage('Please define a Blog Title', 'alert-danger');
                                        }
                                    } else {
                                        $this->addMessage('Passwords does\'t match', 'alert-danger');
                                    }
                                } else {
                                    $this->addMessage('Password can not be empty', 'alert-danger');
                                }
                                break;
                            case 1:
                                $this->addMessage('Username already taken', 'alert-danger');
                                break;
                            case 2:
                                $this->addMessage('Email already taken', 'alert-danger');
                                break;
                            default:
                                $this->addMessage('Invalid data', 'alert-danger');
                        }
                    } else {
                        $this->addMessage('Email is missing', 'alert-danger');
                    }
                } else {
                    $this->addMessage('Username is missing', 'alert-danger');
                }
            } else {
                $this->addMessage('Error during Transmission try again', 'alert-danger');
                genToken('frm_register');
            }
        } else {
            genToken('frm_register');
        }
    }


}