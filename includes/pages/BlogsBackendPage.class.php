<?php
/**
 * Represent the Blog Backend page
 */

class BlogsBackendPage extends BackendPage{

    /**
     * BlogsBackendPage constructor.
     * @param User $user ActualUser
     * @param int $pid Pageid for page Highlight
     */
    public function __construct($user,$pid)
    {
        parent::__construct($user,$pid);
    }

    /**
     * Print the body of the Page
     */
    public function printBody()
    {
        echo '<h2 class="sub-header">Dashboard - Manage Blogs</h2>';
        echo '<div data-type="blog">';
        echo '<input id="searchInput" placeholder="Type To Filter">';
        Dialog::printCrudTable('tblBlog',false,true);
        echo ' <a class="crudCSV btn btn-warning btn-lg" data-id="-1"><span class="glyphicon glyphicon-download-alt"></span> Export as CSV</a>';
        echo '</div>';
    }

    /**
     * Print the Javascript of the page
     */
    public function printScript()
    {
        parent::printScript();
        echo '<script>
                tab="#pageBody";
                pageUrl="backend.php?bodyOnly=0&action=blogs";
              </script>';
        echo '<script src="js/tableFilter.js"></script>';
        echo '<script src="ajax/crud.js"></script>';
        echo '<script src="js/jquery.tablesorter.min.js"></script>';
        echo '<script>$("table").tablesorter();</script>';
    }


}