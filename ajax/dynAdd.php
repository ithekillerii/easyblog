<?php
/**
 * Ajax frontend dynadd Landing.
 * When $_POST['submit_popup'] is not set send only the popup form else send the new page body.
 */
require_once('ajaxBase.inc.php');
if (isset($_POST['submit_popup'])) { //Send Pagebody
    if (isset($_GET['page'], $_GET['id'])) {
        if (checkToken($_POST['token_popup'], 'frm_popup')) {
            if (isset($_POST['title_popup'], $_POST['text_popup'])) {
                if (isset($_SESSION['user'])) {
                    switch ($_GET['page']) {
                        case 'addPost':
                            $blogPage = new BlogPage($_GET['id'], unserialize($_SESSION['user']));
                            if ($blogPage->ajax_addPost($_POST['title_popup'], $_POST['text_popup']) === 0) {
                                //Send the new body to client
                                $blogPage->printBody();
                            } else {
                                //Forbidden
                                sendHeader(403);
                            }
                            break;
                        case 'addComment':
                            $postPage = new PostPage(unserialize($_SESSION['user']));
                            $postPage->ajax_addComment($_POST['title_popup'], $_POST['text_popup']);
                            //Send the new body to client
                            $postPage->printBody();
                            break;
                        default:
                            //Wrong Request
                            sendHeader(400);
                    }
                } else {
                    //Unauthorized
                    sendHeader(401);
                }
            } else {
                //Wrong Request
                sendHeader(400);
            }
        }
    } else {
        //Wrong Request
        sendHeader(400);
    }
} else { //Send Popup
    genToken('frm_popup');
    if (isset($_GET['page'])) {
        switch ($_GET['page']) {
            case 'addPost':
                $title = 'Add Post';
                break;
            case 'addComment':
                $title = 'Add Comment';
                break;
            default:
                //Wrong Request
                sendHeader(400);
        }

        Dialog::printAddEditPopup($title,'Add','frm_popup');
    } else {
        //Wrong Request
        sendHeader(400);
    }
}