<?php
/**
 * Base Includes for ajax Files
 */
define('basePath','../'); //Relative path from script to the root of the Page
require_once('../includes/classLoader.inc.php');
session_start();

/**
 * Send a error header and then exit with code -1
 * @param $headerCode int 400 | 401 | 403
 */
function sendHeader($headerCode)
{
    switch ($headerCode) {
        case 400:
            header($_SERVER["SERVER_PROTOCOL"] . " 400 Bad Request");
            break;
        case 401:
            header($_SERVER["SERVER_PROTOCOL"] . " 401 Unauthorized");
            break;
        case 403:
            header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden");
            break;
    }
    exit(-1);
}