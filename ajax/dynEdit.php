<?php
/**
 * Ajax landing for crud actions
 */
require_once('ajaxBase.inc.php');
$ajaxEdit = new AjaxEdit();
if(isset($_GET['page'],$_SESSION['user'])){
    switch($_GET['page']){
        case 'crudPosts':
            if(isset($_GET['backend'])){
                if(unserialize($_SESSION['user'])->isBackend()){
                    Dialog::printCrudTable('tblPost',false,true);
                }
            }else{
                Dialog::printCrudTable('tblPost');
            }
            break;
        case 'crudComments':
            if(isset($_GET['backend'])){
                if(unserialize($_SESSION['user'])->isBackend()){
                    Dialog::printCrudTable('tblComment',false,true);
                }
            }else{
                Dialog::printCrudTable('tblComment');
            }
            break;
        case 'crudImages':
            if(isset($_GET['backend'])){
                if(unserialize($_SESSION['user'])->isBackend()){
                    Dialog::printCrudTable('tblImage',false,true);
                }
            }else{
                Dialog::printCrudTable('tblImage');
                echo '<button class="crudAdd btn btn-primary btn-lg"><span class="glyphicon glyphicon-upload"></span> Upload new Image</button>';
            }
            break;
        default:
            //Bad Request
            sendHeader(400);
            break;
    }
}else if(isset($_GET['crud'],$_GET['cid'],$_GET['type'],$_SESSION['user'])){
    $ajaxEdit->execCRUD();
}else{
    //Bad Request
    sendHeader(400);
}

