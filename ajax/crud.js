/*
Javascript for the crud tables actions
 */
var reqUrl ='';
var addOn = '';
$(document).on("click",'.closePopup',function(evt){
    evt.preventDefault();
    $("#ajax").hide();
    $("#ajax").text('');
});
//CRUD Show Action
$(document).on('click','.crudShow',function(evt){
    evt.preventDefault();
    var id = $(this).attr('data-id');
    var type = $(this).parents('div').attr('data-type');
    reqUrl="ajax/dynEdit.php?crud=show&type="+type+"&cid="+id+addOn;
    $('#ajax').load(reqUrl).show();
});
//CRUD Edit Action
$(document).on('click','.crudEdit',function(evt){
    evt.preventDefault();
    var id = $(this).attr('data-id');
    var type = $(this).parents('div').attr('data-type');
    reqUrl="ajax/dynEdit.php?crud=edit&type="+type+"&cid="+id+addOn;
    $('#ajax').load(reqUrl).show();
});
//CRUD Delete Action
$(document).on('click','.crudDel',function(evt){
    evt.preventDefault();
    var id = $(this).attr('data-id');
    var type = $(this).parents('div').attr('data-type');
    reqUrl="ajax/dynEdit.php?crud=del&type="+type+"&cid="+id+addOn;
    $('#ajax').load(reqUrl).show();
});
//CRUD Add Action
$(document).on('click','.crudAdd',function(evt){
    evt.preventDefault();
    var id = -1; //$(this).attr('data-id');
    var type = $(this).parents('div').attr('data-type');
    reqUrl="ajax/dynEdit.php?crud=add&type="+type+"&cid="+id+addOn;
    $('#ajax').load(reqUrl).show();
});
//CRUD Export PDF
$(document).on('click','.crudPDF',function(evt){
    evt.preventDefault();
    var id = $(this).attr('data-id');
    var type = $(this).parents('div').attr('data-type');
    reqUrl="export.php?crud=pdf&type="+type+"&cid="+id+addOn;
    window.location.href = reqUrl;
});
//CRUD Export CSV
$(document).on('click','.crudCSV',function(evt){
    evt.preventDefault();
    var id = $(this).attr('data-id');
    var type = $(this).parents('div').attr('data-type');
    reqUrl="export.php?crud=csv&type="+type+"&cid="+id+addOn;
    window.location.href = reqUrl;
});

//Send Form:
$(document).on('click','.confirmPopup',function(evt){
    evt.preventDefault();
    if(reqUrl != null){
        var form_data;
        if($(this).attr('id') === 'submitImage'){
            form_data = new FormData();
            var postdata = ($('#popupForm').serialize()+'&submit_popup=Add').split('&');
            var file_data = $('#imageUploadFile').prop('files')[0];
            form_data.append('file', file_data);
            for(var i = 0;i<postdata.length;i++){
                var val = postdata[i].split('=');
                console.log(val);
                form_data.append(val[0], val[1]);
            }
            $.ajax(reqUrl,{
                method:'POST',
                data: form_data,
                processData: false,
                contentType: false,
                statusCode: {
                    200: function(data) {
                        $("#ajax").hide();
                        $(tab).load(pageUrl);
                        $('#jsMSG').html(data);
                    },
                    400: function(data){
                        $("#ajax").hide();
                        alert("400 Bad request");
                    },
                    401: function(data) {
                        $("#ajax").hide();
                        alert("You have to be logged in (401 Unauthorized)");
                    },
                    403: function(data) {
                        $("#ajax").hide();
                        alert("You have not enough permissions (403 Forbidden)");
                    }
                }
            });
        }else{
            form_data = $('#popupForm').serialize()+'&submit_popup=Add';
            $.ajax(reqUrl,{
                method:'POST',
                data: form_data,
                statusCode: {
                    200: function(data) {
                        $("#ajax").hide();
                        $(tab).load(pageUrl);
                        $('#jsMSG').html(data);
                    },
                    400: function(data){
                        $("#ajax").hide();
                        alert("400 Bad request");
                    },
                    401: function(data) {
                        $("#ajax").hide();
                        alert("You have to be logged in (401 Unauthorized)");
                    },
                    403: function(data) {
                        $("#ajax").hide();
                        alert("You have not enough permissions (403 Forbidden)");
                    }
                }
            });
        }
        console.log(form_data);

    }else{
        $('#popUpMsg').html('<div class="alert alert-danger">You should not be here.<br>Click close</div>');
    }
});