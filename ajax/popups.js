/*
Popup management Javascript
 */
$(document).on("click",'.closePopup',function(evt){
    evt.preventDefault();
    $("#ajax").hide();
    $("#ajax").text('');
});


$(document).on('click','.confirmPopup',function(evt){
    evt.preventDefault();
   if($('input[name=title_popup]').val()==''){
        $('#popUpMsg').html('<div class="alert alert-danger">Title can not be Empty</div>');
   }else if($('textarea[name=text_popup]').val()==''){
        $('#popUpMsg').html('<div class="alert alert-danger">Content can not be empty</div>');
   }else{
       if(reqUrl != null){
           var postdata = $('#popupForm').serialize()+'&submit_popup=Add';
           $.ajax(reqUrl,{
               method:'POST',
               data:postdata,
               statusCode: {
                   200: function(data) {
                       $('main').html(data);
                       $("#ajax").hide();
                   },
                   400: function(data){
                       $("#ajax").hide();
                       alert("400 Bad request");
                   },
                   401: function(data) {
                       $("#ajax").hide();
                       alert("You have to be logged in (401 Unauthorized)");
                   },
                   403: function(data) {
                       $("#ajax").hide();
                       alert("You have not enough permissions (403 Forbidden)");
                   }
               }
           });
       }else{
           $('#popUpMsg').html('<div class="alert alert-danger">You should not be here.<br>Click close</div>');
       }
   }
});