# How to Install EasyBlog
1. Clone the with `git clone https://bitbucket.org/ithekillerii/easyblog.git´ or download the zip.
2. If you download the zip: extract it in your webroot
3. Create a new Database on your Database server
4. Change the database settings in the easyBlog.ini
5. Change the permissions of the images/upload folder so that the Webserver has rwx rights on that folder
6. Import the database.sql from the database folder. This creates the database and inserts the root user with the password `q1w2e3!`
7. If you want some test data in your database import the testData.sql
8. The cleanUpUsers.sql has a procedures that deletes all unregistered users at once