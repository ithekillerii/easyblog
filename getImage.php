<?php
define('basePath','./'); //Relative path from script to the root of the Page
define('imagePath','images/');
define('uploadPath','images/upload/');
require_once('includes/classLoader.inc.php');
/**
 * Returns an Image to be Displayed in the Blog.
 * Quarries the tblImage from the Database to get the dtFilePath from an Image id and sends it to the frontend.
 * When no Image is found it sends the error.png Image.
 * !! This page has no Text output !!
 */
$file = imagePath.'error.jpg';
$id = filter_input(INPUT_GET,'id',FILTER_VALIDATE_INT,FILTER_NULL_ON_FAILURE);
if(isset($id)){
    //Get fix defined Images
    if($id <= 0){
        switch($id){
            case 0:
                //Logo
                $file = imagePath.'logo.png';
                break;
            default:
                break;
        }
    }else{
        $image = Image::createFromID($id);
        if(isset($image)){
            $file = uploadPath.$image->getDtFilePath();
        }
    }
}
$filetype = mime_content_type($file);
header('Content-Type: '.$filetype);
header('Content-Length: ' . filesize($file));
readfile($file);