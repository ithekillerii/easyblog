+ EasyBlog
EasyBlog is a light blogging Software with a mysql Database and user Management.
++ Featureset:
    - Frontend:
        * Blogging is realized per user so every user has one Blog and can comment on other Blogs/Posts.
        * User has to register guest only can read.
        * User self Management:
            ** Change name
            ** Change email
            ** Change password
        * Add / Edit / Delete own Images
    - Backend:
        * Add / Edit / Delete all Users , Blogs and Posts
        * Can nominate other Backend Users
        * Add / Edit / Delete all Images
        * Created after Frontend
    - Install script:
        * Optional
        * Create database
        * Create first Backend user
++ Additional Information:
* Configuration file:       easyBlog.ini
* Database configuration:   database/database.sql
* Upload Directory:         images/upload
* Public Git Repo:          https://bitbucket.org/ithekillerii/easyblog
* Database uses sha1 as hashing

++ Licenses + 3 party
* SourcecodePro Font is an open font from
https://store2.adobe.com/cfusion/store/html/index.cfm?store=OLS-DE&event=displayFontPackage&code=1960
* Parsedown: http://parsedown.org