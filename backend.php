<?php
define('page_backend', 'true');
define('frmFrontend', 'backendLogin');
require_once('includes/base.inc.php');
//Send only body for ajax requests
if (isset($_GET['bodyOnly'])) {
    if (isset($_SESSION['user'])) {
        if (unserialize($_SESSION['user'])->isBackend()) {
            $page->printBody();
        } else {
            header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden");
        }
    } else {
        header($_SERVER["SERVER_PROTOCOL"] . " 401 Unauthorized");
    }
    exit(0);
}
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>EasyBlog::Backend</title>
    <?php
    $page->inc_bs_Head();
    ?>
    <link href="css/dashboard.css" rel="stylesheet">
</head>
<body>
<?php
if (isset($_SESSION['user'])) {
    if (unserialize($_SESSION['user'])->isBackend()) {
        include_once('includes/nav.inc.php');
        ?>
        <main class="container-fluid">
            <div class="row">
                <?php $page->printHeader(); ?>
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <h1 class="page-header">Backend</h1>
                    <div id="jsMSG"></div>
                    <div id="pageBody">
                        <?php $page->printBody(); ?>
                    </div>
                </div>
            </div>
        </main>
        <div id="ajax"></div>
        <?php
    } else {
        destroySession();
        ?>
        <header class="jumbotron">
            <div class="alert alert-dander">
                <p>You have no permissions to be here. If this is an error please contact an administrator</p>
                <p><a class="btn btn-warning btn-lg" href="index.php">Beam me out scotty <span class="glyphicon glyphicon-open"></span></a></p>
            </div>
        </header>
        <?php
    }
} else {
    genToken(frmFrontend);
    ?>
    <div class="container">
        <form method="post" action="" class="form-horizontal">
            <h2 class="form-signin-heading">Please sign in</h2>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="inputUsername">Username</label>

                <div class="col-sm-10">
                    <input type="text" id="inputUsername" class="form-control" placeholder="Username" required autofocus
                           name="username">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-sm-2 control-label">Password</label>

                <div class="col-sm-10">
                    <input type="password" id="inputPassword" class="form-control" placeholder="Password" required
                           name="password">
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-lg btn-primary" type="submit" name="<?= frmFrontend ?>_submit">Sign in</button>
                <input type="hidden" name="<?= frmFrontend ?>_token" value="<?= getToken(frmFrontend) ?>">
            </div>
        </form>
    </div>
    <?php
}
?>
<?php
$page->inc_bs_Foot();
$page->printScript();
?>
</body>
</html>