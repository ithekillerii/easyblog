/**
 * Filter table
 */
$("#searchInput").keyup(function() {
    if(this.value == ''){
        $("tbody").find("tr").show();
    }else{
        var rows = $("tbody").find("tr").hide();
        var data = this.value.split(" ");
        $.each(data, function(i, v) {
            rows.filter(":contains('" + v + "')").show();
        });
    }
});