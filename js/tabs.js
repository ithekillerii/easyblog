/**
 * Javascript for the tabs
 */
var pageUrl;
var tab;

$('#settingsTabs li a').click(function(evt){
    evt.preventDefault();
    $('#settingsTabs li').removeClass('active');
    $(this).parent().addClass('active');
    $('.tabBody').hide();
    tab = '#' + $(this).attr('data-ref');
    switch (tab){
        case '#postsInfo':
            if($(tab).text()==''){
                pageUrl = 'ajax/dynEdit.php?page=crudPosts';
                $(tab).load(pageUrl,function( response, status, xhr ) {
                    if ( status == "error" ) {
                        var msg = "Sorry but there was an error: ";
                        $( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
                    }else{
                        $("#postsInfo table").tablesorter({
                            headers: {
                                3: {
                                    sorter: false
                                }
                            }
                        });
                    }
                });
            }
            break;
        case '#commentsInfo':
            if($(tab).text()==''){
                pageUrl = 'ajax/dynEdit.php?page=crudComments';
                $(tab).load(pageUrl,function(){
                    $("#commentsInfo table").tablesorter({
                        headers: {
                            3: {
                                sorter: false
                            }
                        }
                    });
                });
            }
            break;
        case '#imagesInfo':
            if($(tab).text()==''){
                pageUrl = 'ajax/dynEdit.php?page=crudImages';
                $(tab).load(pageUrl,function(){
                    $("#imagesInfo table").tablesorter({
                        headers: {
                            4: {
                                sorter: false
                            }
                        }
                    });
                });
            }
            break;
        default :
            break;
    }
    $.getScript('ajax/crud.js');
    $(tab).show();
});
/*
$('#reject').click(function(evt){
    evt.preventDefault();
    window.location.reload();
});
*/


