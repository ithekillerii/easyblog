/*Main Js file*/
var reqUrl = null;
var id=getUrlParameter('id');

function requestAjax(reqUrl) {
    if ($('#ajax').text() == '') {
        $.get(reqUrl, function (data) {
            $('#ajax').html(data);
        });
        $.getScript('ajax/popups.js');
    }
    $('#ajax').show();
}

function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}

$(document).keyup(function(evt) {
    if(evt.keyCode == 27){ //Esc Button pressed
        $('#ajax').hide().text('');
    }
});

$('#addPost').click(function () {
    reqUrl = 'ajax/dynAdd.php?page=addPost&id='+id;
    requestAjax(reqUrl);
});

$('#addComment').click(function () {
    reqUrl = 'ajax/dynAdd.php?page=addComment&id='+id;
    requestAjax(reqUrl);
});