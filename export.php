<?php
/**
 * Return a PDF File
 */
define('page_backend', 'false');
define('frmFrontend', 'frontendLogin');
require_once('includes/base.inc.php');
require_once('includes/tcpdf/tcpdf.php');

/**
 * Class ExportPDF
 * My export Class for the PDF's
 */
class ExportPDF extends TCPDF{

    /**
     * ExportPDF constructor.
     */
    public function __construct()
    {
        parent::__construct(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    }

    /**
     * Header of the PDF Pages
     */
    public function Header() {
        // Set font
        $this->SetFont('helvetica', 'B', 20);
        $this->SetY(15);
        // Title
        $this->Cell(0, 15, 'EasyBlog Export', 1, false, 'C', 0, '', 0, false, 'M', 'M');
    }

    /**
     * Footer of the PDF Pages
     */
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

    /**
     * Standard Initialisation
     */
    public function config(){
        // set document information
        $this->SetCreator(PDF_CREATOR);
        $this->SetAuthor('EasyBlog');
        $this->SetTitle('EasyBlog Export');
        $this->SetSubject('PDF export');
        $this->SetKeywords('EasyBlog, export');

        // set default header data
        $this->SetHeaderData('', 300, 'EasyBlog Export');

        // set header and footer fonts
        $this->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $this->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $this->SetHeaderMargin(PDF_MARGIN_HEADER);
        $this->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $this->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $this->setFontSubsetting(true);
        $this->SetFont('dejavusans', '', 14, '', true);
        $this->AddPage();
    }

    /**Get the Absolute Web path to this file of EasyBlog
     * @return String Absolute Web path to this file of EasyBlog
     */
    public function getAbsWebPath(){
        $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[PHP_SELF]";
        return preg_replace('/export.php$/', '', $actual_link);
    }

}

$id = filter_input(INPUT_GET,'cid',FILTER_VALIDATE_INT,FILTER_NULL_ON_FAILURE);
if (isset($_GET['crud'], $id, $_GET['type'])) {

    switch ($_GET['type']) {
        case 'user':
            if (unserialize($_SESSION['user'])->isBackend()) {
                if ($_GET['crud'] === 'csv') {
                    $db = Database::iniStandard();
                    $db->addTableDescription('tblUser', array());
                    echo '<pre>' . implode("\n", $db->getCSVArray()) . '</pre>';
                } else {
                    echo '<h1>This Export config is not available</h1>';
                }
            } else {
                echo '<h1>You have not enough rights</h1>';
            }
            break;
        case 'blog':
            if (unserialize($_SESSION['user'])->isBackend()) {
                if ($_GET['crud'] === 'csv') {
                    $db = Database::iniStandard();
                    $db->addTableDescription('tblBlog', array());
                    echo '<pre>' . implode("\n", $db->getCSVArray()) . '</pre>';
                } else {
                    echo '<h1>This Export config is not available</h1>';
                }
            } else {
                echo '<h1>You have not enough rights</h1>';
            }
            break;
        case 'post':
            if ($_GET['crud'] === 'pdf') {
                $pdf = new ExportPDF();
                $pdf->config();
                $post = Post::createFromID($id);
                $htmlpre = $post->toHtml();
                $html = preg_replace('/<img src="/','<img src="'.$pdf->getAbsWebPath(),$htmlpre);
                $html .= '<hr><br><h1>Comments:</h1>'.$post->getComments();
                $pdf->writeHTML($html, true, false, true, false, '');
                $pdf->Output('exportPost.pdf', 'I');
            } else {
                echo '<h1>This Export config is not available</h1>';
            }
            break;
        case 'comment':
            if ($_GET['crud'] === 'pdf') {
                $pdf = new ExportPDF();
                $pdf->config();
                $comment = Comment::createFromID($id);
                $html = $comment->toHtml();
                $pdf->writeHTML($html, true, false, true, false, '');
                $pdf->Output('exportComment.pdf', 'I');
            } else {
                echo '<h1>This Export config is not available</h1>';
            }
            break;

    }
} else {
    echo '<h1>You are wrong here</h1>';
}