<?php
define('page_backend','false');
define('frmFrontend','frontendLogin');
require_once('includes/base.inc.php');
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>EasyBlog</title>
    <?php
    $page->inc_bs_Head();
    ?>
</head>
<body>
<?php include_once('includes/nav.inc.php'); ?>
<header class="jumbotron">
    <div class="container">
        <?php $page->printHeader(); ?>
    </div>
</header>
<main class="container">
    <div id="#jsMSG"></div>
    <?php $page->printBody(); ?>
</main>
<div id="ajax"></div>
<?php
$page->inc_bs_Foot();
$page->printScript();
?>
</body>
</html>