/*
Insert some test data
 */

INSERT INTO tblUser(idUser,dtUsername, dtPassword, dtSalt, dtEmail, dtFirstName, dtLastName, dtBackend, dtLocked)
    VALUES (2,'sam',sha1('salt1234'),'salt','sam@hobbi.net','Samweis','Gamdschie','false','false');

INSERT INTO tblBlog(idBlog,dtTitle,dtDescription,fiUser)
    VALUES (1,'Welcome','Welcome to EasyBlog',1),
           (2,'The Quest','My Blog about the Quest',2);

INSERT INTO tblPost(idPost, dtTitle, dtContent, fiBlog)
    VALUES (1,'Welcome','<h1>Welcome to EasyBlog</h1><p>Hello and welcome to EasyBlog the simple Blog Engine</p>',1),
           (2,'ParseDown','<h1>EasyBlog uses Markdown</h1><p>EasyBlog uses the ParseDown Engine to turn your Markup into HTML</p>',1),
           (3,'Hello my Name is Sam','<h1>Hello</h1><p>Hello I am Samweis Gamdschie a friend of Frodo</p>',2),
           (4,'Frodo found the Ring','<h1>Frodo and the Ring</h1><p>Today Frodo found the Ring</p>',2);

INSERT INTO tblComment(idComment, dtTitle, dtContent, fiPost, fiUser)
    VALUES (1,'Welcome','Welcome Sam to EasyBlog',3,1),
           (2,'THX','thanks',3,2),
           (3,'Have you some tips','Have you some tips about markdown ?',2,2),
           (4,'Nice to know','# produces a h1 tag',2,1),
           (5,'I want also one','I want also a ring',4,1),
           (6,'That is not so good','The ring is magical',4,2);

INSERT INTO tblImage (idImage, dtName, dtCaption, dtFilePath, fiUser)
    VALUES (1,'test.jpg','Test Image','../testImages/test.jpg',1),
           (2,'test2.png','Test2 Images','../testImages/test2.png',1),
           (3,'ring.png','The Ring','../testImages/ring.png',2);