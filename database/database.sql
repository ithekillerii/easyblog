/*
Use this script to build your database
 */


/*
Drop All tables
 */
DROP TABLE IF EXISTS tblImage;
DROP TABLE IF EXISTS tblComment;
DROP TABLE IF EXISTS tblPost;
DROP TABLE IF EXISTS tblBlog;
DROP TABLE IF EXISTS tblUser;

CREATE TABLE IF NOT EXISTS tblUser (
  idUser INT UNSIGNED NOT NULL AUTO_INCREMENT,
  dtUsername VARCHAR(45) NOT NULL,
  dtPassword VARCHAR(40) NOT NULL,
  dtSalt VARCHAR(20) NOT NULL,
  dtEmail VARCHAR(45) NOT NULL,
  dtFirstName VARCHAR(45) NULL,
  dtLastName VARCHAR(45) NULL,
  dtBackend ENUM('false','true') NOT NULL DEFAULT 'false',
  dtLocked ENUM('false','true','unregister') NOT NULL DEFAULT 'false',
  dtTSCreation TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  dtTSUpdate TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  dtTSLastLogin TIMESTAMP NULL DEFAULT 0,
  PRIMARY KEY (idUser),
  UNIQUE INDEX dtUsername_UNIQUE (dtUsername ASC),
  UNIQUE INDEX dtEmail_UNIQUE (dtEmail ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table tblBlog
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS tblBlog (
  idBlog INT UNSIGNED NOT NULL AUTO_INCREMENT,
  dtTitle VARCHAR(45) NULL,
  dtDescription VARCHAR(120) NULL,
  dtTSCreation TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  dtTSUpdate TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  fiUser INT UNSIGNED NOT NULL,
  PRIMARY KEY (idBlog),
  INDEX fk_tblBlog_tblUser_idx (fiUser ASC),
  CONSTRAINT fk_tblBlog_tblUser
    FOREIGN KEY (fiUser)
    REFERENCES tblUser (idUser)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table tblPost
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS tblPost (
  idPost INT UNSIGNED NOT NULL AUTO_INCREMENT,
  dtTitle VARCHAR(45) NULL,
  dtContent TEXT NULL,
  dtTSCreation TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  dtTSUpdate TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  fiBlog INT UNSIGNED NOT NULL,
  PRIMARY KEY (idPost),
  INDEX fk_tblPost_tblBlog1_idx (fiBlog ASC),
  CONSTRAINT fk_tblPost_tblBlog1
    FOREIGN KEY (fiBlog)
    REFERENCES tblBlog (idBlog)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table tblComment
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS tblComment (
  idComment INT UNSIGNED NOT NULL AUTO_INCREMENT,
  dtTitle VARCHAR(45) NULL,
  dtContent VARCHAR(120) NULL,
  dtTSCreation TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  dtTSUpdate TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  fiPost INT UNSIGNED NOT NULL,
  fiUser INT UNSIGNED NOT NULL,
  PRIMARY KEY (idComment),
  INDEX fk_tblComment_tblPost1_idx (fiPost ASC),
  INDEX fk_tblComment_tblUser1_idx (fiUser ASC),
  CONSTRAINT fk_tblComment_tblPost1
    FOREIGN KEY (fiPost)
    REFERENCES tblPost (idPost)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_tblComment_tblUser1
    FOREIGN KEY (fiUser)
    REFERENCES tblUser (idUser)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table tblImage
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS tblImage (
  idImage INT UNSIGNED NOT NULL AUTO_INCREMENT,
  dtName VARCHAR(45) NOT NULL,
  dtCaption VARCHAR(45) NULL,
  dtFilePath VARCHAR(45) NOT NULL,
  fiUser INT UNSIGNED NOT NULL,
  dtTSCreation TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  dtTSUpdate TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (idImage),
  INDEX fk_tblImage_tblUser1_idx (fiUser ASC),
  CONSTRAINT fk_tblImage_tblUser1
    FOREIGN KEY (fiUser)
    REFERENCES tblUser (idUser)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

/* Insert Root user */
INSERT INTO tblUser(idUser,dtUsername, dtPassword, dtSalt, dtEmail, dtFirstName, dtLastName, dtBackend, dtLocked)
  VALUES (1,'root',sha1('saltq1w2e3!'),'salt','root@root','root','admin','true','false');