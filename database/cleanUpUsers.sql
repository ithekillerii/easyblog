/*----------------------------------------------------------------------------
| Routine : cleanupUsers
| Author(s)  : (c) BTSi Christophe LAURES
| CreateDate : 2015-06-16
|
| Description : Clean up the Unregistered users
|
| Parameters :
| ------------
|  IN  : <none>
|
|  OUT : text :List with Usernames
|
| stdReturnValues :
| -----------------
|   none
|
| History of change(s) (yyyy-mm-dd)
| -------------------
|  2015-05-20 Christophe : creation
|
| List of callers : (this routine is called by the following routines)
| -----------------
|
|---------------------------------------------------------------------------*/
DROP PROCEDURE IF EXISTS cleanupUsers;

DELIMITER $$

CREATE PROCEDURE cleanupUsers(OUT userList TEXT)

  COMMENT 'Clean up the Users'

  BEGIN
    DECLARE Cursor_end TINYINT;
    /*Intern Usage Varibles for cursor*/
    DECLARE _usrName VARCHAR(45);
    DECLARE _numBackendUser INT;
    DECLARE _output TEXT;

    DECLARE userCursor CURSOR FOR SELECT dtUsername FROM tblUser WHERE dtLocked = 'unregister';
    DECLARE CONTINUE HANDLER
    FOR NOT FOUND SET Cursor_end = 1;

    SET Cursor_end = 0 ;
    SET _output = '';
    SELECT COUNT(*) INTO _numBackendUser FROM tblUser WHERE dtBackend = 'true' AND dtLocked != 'unregister';

    OPEN userCursor;

    cursorLoop: LOOP

      FETCH userCursor INTO _usrName;

      SELECT CONCAT(_output,', ',_usrName) INTO _output;

      IF Cursor_end = 1 THEN
        LEAVE cursorLoop;
      END IF;

    END LOOP cursorLoop;

    IF _numBackendUser > 0 THEN
      DELETE FROM tblUser WHERE dtLocked = 'unregister';
    ELSE
      SELECT CONCAT(_output,', BACKEND USERES NOT DELETED') INTO _output;
      DELETE FROM tblUser WHERE dtLocked = 'unregister' AND dtBackend = 'false';
    END IF ;

    SET userList = _output;

  END $$

DELIMITER ;